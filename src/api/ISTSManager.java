package api;

import model.Vo.VOParada;
import model.data_structures.ArbolRojoNegro;
import model.data_structures.IList;
public interface ISTSManager {
	/**
	 * M�todo para cargar toda la informaci�n
	 * @throws Exception 
	 */
	public void cargar() throws Exception;
	/**
	 * M�todo para contar los componentes conectados del grafo
	 */
	public int contarComponentesConectados();
	/**
	 * M�todo para encontrar los componentes conectados a una parada dada
	 */
	public ArbolRojoNegro componentesConectadosAUnaParada(int idParada);
	/**
	 * Mira si el grafo tiene un menos un ciclo
	 * @return
	 */
	public IList<VOParada> tieneCiclos();
	
}