package model.Vo;
import java.util.Arrays;

import model.data_structures.DoubleLinkedList;
public class VOCalendar{
	private Integer serviceId;
	private Integer[] dias;
	private VODate inicio;
	private VODate fin;
	private DoubleLinkedList<VODate> excepciones;
	public VOCalendar(Integer serviceId, Integer[] dias, VODate inicio, VODate fin) {
		super();
		this.serviceId = serviceId;
		this.dias = dias;
		this.inicio = inicio;
		this.fin = fin;
		excepciones= new DoubleLinkedList<VODate>();
	}
	public Integer[] getDias() {
		return dias;
	}
	public void setDias(Integer[] dias) {
		this.dias = dias;
	}
	public void agregarExcepcion(VODate excepcion){
		this.excepciones.addAtEnd(excepcion);
	}
	
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public DoubleLinkedList<VODate> getExcepciones() {
		return excepciones;
	}
	public void setExcepciones(DoubleLinkedList<VODate> excepciones) {
		this.excepciones = excepciones;
	}
	@Override
	public String toString() {
		return "VOCalendar [serviceId=" + serviceId + ", dias=" + Arrays.toString(dias) + ", inicio=" + inicio
				+ ", fin=" + fin + "]";
	}
	
}