package model.Vo;
public class VOStatus implements Comparable<VOStatus>{
	private String status;
	private VOHour expectedLeaveTime;
	private VOHour lastUpdateTime;
	private VOParada parada;
	
	public VOStatus(String status, String lineaHoraELT, String lineaHoraLastUpdate, VOParada parada) {
		this.status=status;		
		String temporalidad=lineaHoraELT.split(" ")[0].trim().split(":")[1].trim().substring(2).trim();
		String hora1= lineaHoraELT.split(" ")[0].trim().split(":")[0];
		
		VOHour nueva = new VOHour((temporalidad.equals("am"))?Integer.parseInt(hora1):(Integer.parseInt(hora1)+12),
				(lineaHoraELT.split(" ")[0].trim().split(":").length==2)? Integer.parseInt(lineaHoraELT.split(" ")[0].trim().split(":")[1].substring(0, 2)):(Integer.parseInt(lineaHoraELT.split(" ")[0].trim().split(":")[1])),
						(lineaHoraELT.split(" ")[0].trim().split(":").length==2)? 0:(Integer.parseInt(lineaHoraELT.split(" ")[0].trim().split(":")[2].substring(0, 2))));
		expectedLeaveTime= nueva;
		
		String temporalidad1= lineaHoraLastUpdate.trim().split(" ")[1].trim();
		String horas[] = lineaHoraLastUpdate.trim().split(" ")[0].trim().split(":");
		int hora = (temporalidad1.equals("am"))?Integer.parseInt(horas[0].trim()):Integer.parseInt(horas[0].trim())+12;
		int minutos = Integer.parseInt(horas[1].trim());
		int segundos = Integer.parseInt(horas[2].trim());
		VOHour nueva1 = new VOHour(hora, minutos, segundos);
		lastUpdateTime=nueva1;
		this.parada = parada;
	}		
	public String getStatus() {
		return status;
	}
	
	@Override
	public int compareTo(VOStatus o) {
		return lastUpdateTime.compareTo(o.lastUpdateTime);
	}
}
