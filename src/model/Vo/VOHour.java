package model.Vo;
public class VOHour implements Comparable<VOHour>{
	private int hour;
	private int minute;
	private int seconds;
	public VOHour(int hour, int minute, int seconds) {
		super();
		this.hour = hour;
		this.minute = minute;
		this.seconds = seconds;
	}
	public String toString() {
		return "[hour=" + hour + ", minute=" + minute + ", seconds=" + seconds + "]";
	}
	
	public void setHour(int hour)
	{
		this.hour = hour;
	}
	
	public int getHour() {
		return hour;
	}
	public int getMinute() {
		return minute;
	}
	public int getSeconds() {
		return seconds;
	}
	
	public void increaseHour()
	{
		hour++;
		revisar();
	}
	
	public void decreaseHour()
	{
		hour--;
		revisar();
	}
	
	public void increaseMinute()
	{
		minute++;
		revisar();
	}
	
	public void decreaseMinute()
	{
		minute--;
		revisar();
	}
	
	public void increaseSeconds()
	{
		seconds++;
		revisar();
	}
	
	public void decreaseSeconds()
	{
		seconds--;
		revisar();
	}
	
	public void revisar()
	{
		if(seconds == 60)
		{
			seconds = 0;
			minute++;
		}
		if(seconds == -1)
		{
			seconds = 59;
			minute--;
		}
		if(minute == 60)
		{
			minute = 0; 
			hour ++;
		}
		if(minute == -1)
		{
			minute = 59;
			hour--;
		}
		if(hour == 24)
			hour = 0;
		if(hour == -1)
			hour = 23;
	
	}
	public int minutesTo(VOHour hora){
		return Math.abs((this.hour-hora.hour*60)+this.minute-hora.minute);
	}
	
	@Override
	public int compareTo(VOHour o) {		
		return (this.hour-o.hour==0)?((this.minute-o.minute==0)?((this.seconds==o.seconds)?0:this.seconds-o.seconds):this.minute-o.minute):this.hour-o.hour;
	}	
}