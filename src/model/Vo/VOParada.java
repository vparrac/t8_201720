package model.Vo;
/**
 * Representation of a Stop object
 */
public class VOParada implements Comparable<VOParada>{	
	private int stop_id;

	public VOParada(int stop_id) {
		super();
		this.stop_id = stop_id;
	}
	public int getStop_id() {
		return stop_id;
	}	
	@Override
	public String toString() {
		return "VOParada [stop_id=" + stop_id + "]";
	}
	@Override
	public int compareTo(VOParada arg0) {
		// TODO Auto-generated method stub
		return 0;
	}	
}