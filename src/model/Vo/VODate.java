package model.Vo;
public class VODate implements Comparable<VODate>{
	private int year;
	private int month;
	private int day;
	public VODate(int year, int month, int day) {
		super();
		this.year = year;
		this.month = month;
		this.day = day;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	@Override
	public String toString() {
		return "VODate [year=" + year + ", month=" + month + ", day=" + day + "]";
	}
	@Override
	public int compareTo(VODate o) {		
		return ((year-o.year)==0)?(((month-o.month)==0)?day-o.day:month-o.month):year-o.year;
	}
}