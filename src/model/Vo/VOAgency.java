package model.Vo;
public class VOAgency {	
	private String id;
	private String name;
	private String url;
	private String timeZone;
	private String lang;
	public VOAgency(String id, String name, String url, String timeZone, String lang) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.timeZone = timeZone;
		this.lang = lang;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	@Override
	public String toString() {
		return "VOAgency {id=" + id + ", name=" + name + "}";
	}		
}
