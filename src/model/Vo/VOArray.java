package model.Vo;

import java.util.Iterator;

public class VOArray <O> implements Iterable<O>{
	private final static int CAPACIDAD_INICIAL =1000;
	private int size;

	private int number;
	private int current;
	private O[] elements;
	private int n;
	/**
	 * Constructor de la clase, capacidad inicial =100
	 */
	public VOArray(){
		number=0;
		size=1000;
		current=0;
		O[] os = (O[]) new Object[CAPACIDAD_INICIAL];
		elements = os; 
	}
	/**
	 * Retorna el tama�o del arreglo
	 */
	public int size(){
		return size;
	}
	
	public int number()
	{
		return number;
	}
	/**
	 * A�ade un elemento al arreglo
	 */
	public void add(O object){
		number++;
		if(number==size) resize();
		elements[current] =object;		
		current++;
	}
	/**
	 * Doblega el tama�o de la lista
	 */
	private void resize(){
		O[] os = (O[]) new Object[size*2];
		for (int i = 0; i < elements.length; i++) {			
			os[i] = elements[i];
		}	
		size= (size*2);
		elements=os;
	}
	public O get(int index) throws IndexOutOfBoundsException {
		if(index>=size){
			throw new IndexOutOfBoundsException();
		}
		return elements[index];
	}

	public Object[] getElements(){
		return elements;
	}


	@Override
	public Iterator<O> iterator() {
		O[] nuevo = (O[]) new Object[number];
		for (int i = 0; i < number; i++) {
			nuevo[i]=elements[i];
		}

		return new model.data_structures.Iterator(nuevo);
	}
//	public static void main(String[] args) {
//		VOArray<Integer> prueba = new VOArray<>();
//		
//		
//		for (Integer integer : prueba) {
//			System.out.println(integer);
//			System.out.println("Esto es una prueba");
//		}
//	}
}