package model.logic;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

import org.omg.PortableInterceptor.DISCARDING;

import api.ISTSManager;
import model.Vo.VOParada;
import model.data_structures.ArbolRojoNegro;
import model.data_structures.CC;
import model.data_structures.Cycle;
import model.data_structures.DirectedGraph;
import model.data_structures.IList;
public class STSManager implements ISTSManager{
	private DirectedGraph<Integer, VOParada> graph;
	public void cargar() throws Exception {
		try{
			FileReader fr=new FileReader("./data/csv/stop_times.txt");
			BufferedReader br = new BufferedReader(fr);
			String lin=br.readLine();
			lin=br.readLine();
			String[] ln = lin.split(",");
			//Crea el grafo
			graph = new DirectedGraph<>();
			int idOrigen = Integer.parseInt(ln[3]);
			int idParada;
			VOParada parada;
			while(lin != null){		
				ln = lin.split(",");
				if(ln.length==8){
					idOrigen= Integer.parseInt(ln[3]);
				}						
				idParada= Integer.parseInt(ln[3]);
				//Crea una parada
				parada= new VOParada(idParada);
				//A�ade al grafo el vertice
				if(!graph.getVertices().contains(new Integer(idParada)))
					graph.addVertice(parada.getStop_id(), parada);		
				
				//A�ade el arco
				graph.addEdge(idOrigen, idParada, ((ln.length==9)?Double.parseDouble(ln[8]):0));
				idOrigen= idParada;
				lin=br.readLine();				
			}
			br.close();
			fr.close();
		}		
		catch
		(IOException e) {
			throw new Exception("Ha ocurrido un error " +e.getMessage());
		}		
	}
	@Override
	public int contarComponentesConectados() {
		CC cc = new CC<>(graph);
		cc.generateConnectedComponents();
		return cc.nConnectedComponents();		
	}
	@Override
	public ArbolRojoNegro componentesConectadosAUnaParada(int idParada) {
		model.data_structures.Paths paths = new model.data_structures.Paths(graph, idParada);
		paths.generatePaths();	
		return paths.getRutas();
		
		
	}
	@Override
	public IList<VOParada> tieneCiclos(){
		Cycle cycle = new Cycle<>(graph);
		cycle.sourceCycles();		
		if(!cycle.hasCycles()){
			return null;			
		}
		else{
		return (IList<VOParada>) cycle.getCycles().getElement(0);
		}
	}
		
}