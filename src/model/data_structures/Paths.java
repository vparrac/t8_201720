package model.data_structures;


//Implementado con algoritmo Breadth-First Search
public class Paths<K extends Comparable<K>,V>{
	
	private K[] vertices;
	private K[] edgeFrom;
	private int[] distFrom; //nArcosDesdeOrigen
	
	private DirectedGraph<K,V> grafo;
	private K origin;
	
	public Paths(DirectedGraph<K,V> grafo, K origin)
	{
		this.grafo = grafo;
		this.origin = origin;
		
		IList<Vertice<K,V>> verticesList = grafo.getVertices().list();
		vertices = (K[]) new Comparable[verticesList.size()];
		edgeFrom = (K[]) new Comparable[verticesList.size()];
		distFrom = new int[verticesList.size()];
		int i = 0;
		for(Vertice<K,V> actual : verticesList)
		{
			vertices[i] = actual.getKey();
			edgeFrom[i] = null;
			distFrom[i]	 = -1;
			i++;
		}
	}
	
	/**
	public void generatePaths()
	{
		int posi = buscarPos(origin);
		if(posi == -1)
			return;
		IQueue<K> cola = new Queue<K>();
		cola.enqueue(origin);
		distFrom[posi] = 0;
		
		generatePaths(cola);
		
	}
	
	private void generatePaths(IQueue<K> cola)
	{
		if(cola.isEmpty())
			return;
		
		K actual = cola.dequeue();
		IList<Edge<K>> arcos = grafo.getEdges(actual);
		for(Edge<K> arcoActual : arcos)
		{
			int posi = buscarPos(arcoActual.getDest());
			if((edgeFrom[posi] == null && !vertices[posi].equals(origin) || distFrom[posi] > arcoActual.getWeight()))
			{
				if(edgeFrom[posi] == null)
					cola.enqueue(vertices[posi]);
				
				edgeFrom[posi] = actual;
				distFrom[posi] = arcoActual.getWeight();
				print();
			}
		}
		generatePaths(cola);
	}
	
	*/
	
	private int buscarPos(K key)
	{
		for(int i = 0; i < vertices.length; i++)
			if(vertices[i].equals(key))
				return i;
		return -1;//posible error.
	}
	
	public boolean hasPathTo(K key)
	{
		int posi = buscarPos(key);
		return (posi == -1)? false : (edgeFrom[posi] != null)? true : false;
	}
	
	public IList<K> pathTo(K key)
	{
		int posi = buscarPos(key);
		if(posi == -1)
			return null;
		
		IList<K> path = new DoubleLinkedList<K>();
		
		path.add(key);
		if(hasPathTo(key))
			pathTo(key, path);
		return path;
		
	}
	
	private void pathTo(K actual, IList<K> path)
	{
		int posi = buscarPos(actual);
		K source = edgeFrom[posi];
		path.add(source);
		if(source.equals(origin))
			return;
		pathTo(source, path);
	}
	
	public void generatePaths()
	{
		distFrom[buscarPos(origin)] = 0;
		generatePaths(origin, new Queue<K>());
	}
	
	private void generatePaths(K actual, Queue<K> cola)
	{
		//print();
		IList<K> adjsActual = grafo.adj(actual);
		
		int nEdges = (adjsActual != null)? adjsActual.size() : 0;
		int posActualAdj;
		int distEdgeFromActual;//Distancia total por edge de 'actual' hasta origin.
		
		for(K actualAdj : adjsActual)
		{
			
			posActualAdj = buscarPos(actualAdj);
			if(edgeFrom[posActualAdj] == null)
				cola.enqueue(actualAdj);
			distEdgeFromActual = calculateDist(actual);
			
			if(edgeFrom[posActualAdj] == null || distFrom[posActualAdj] > distEdgeFromActual)
			{
				edgeFrom[posActualAdj] = actual;
				distFrom[posActualAdj] = distEdgeFromActual;
			}
		}
		
		//Pruebas:
			//System.out.println("Cola: " + cola.toString());
			//System.out.println("Size adjs Actual: " + ((adjsActual != null)? adjsActual.size() : -1));
		//
		K actualAdj;
		while(nEdges > 0 && cola.size() > 0)
		{
			actualAdj = cola.dequeue();
			generatePaths(actualAdj, cola);
			nEdges--;
		}
	}
	
	
	private int calculateDist(K source)
	{
		if(source.equals(origin))
			return 1;
		return distFrom[buscarPos(source)] +1;
	}
	
	public ArbolRojoNegro<Integer, IList<K>> getRutas()
	{
		ArbolRojoNegro<Integer, IList<K>> arbolRutasPorDistancia = new ArbolRojoNegro<Integer, IList<K>>();
		
		int distArcosMax = 0;
		for(int i = 0; i < vertices.length; i++)
			if(distFrom[i] > distArcosMax)
				distArcosMax = distFrom[i];
		
		IList<K> actual;
		for(int x = 0; x <= distArcosMax; x++)
		{
			actual = new DoubleLinkedList<K>();
			for(int i = 0; i < vertices.length; i++)
			{
				if(edgeFrom[i] != null)
					if(distFrom[i] == x)
						actual.add(vertices[i]);
			}
			arbolRutasPorDistancia.add(x, actual);
		}
		
		return arbolRutasPorDistancia;
		
	}

	public void print()
	{
		System.out.println("+++++++++++++");
		for(int i = 0; i < vertices.length; i++)
		{
			System.out.println("|"+vertices[i].toString()+"|"+edgeFrom[i]+"|"+distFrom[i]+"|");
		}
	}
}
