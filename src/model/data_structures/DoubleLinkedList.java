package model.data_structures;
import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;
public class DoubleLinkedList<T> implements IList <T>, Serializable {
	private static final long serialVersionUID = 1L;
	private int n;
	private Node<T> head;
	private Node<T> last;
	private Node<T> current;
	public DoubleLinkedList() {		
		this.n = 0;	
		head=null;
		last=null;
		current=null;
	}
	public int size() {return n;}
	public T getNext() throws NoSuchElementException{
		if(isEmpty()&&n==0){
			throw new NoSuchElementException();
		}
		if(current==null&&n!=0){
			current= head;
			return head.getValue();
		}
		else{
			current=current.getNext();
			return current.getValue();
		}		
	}
	public int getPosicionElemento(T pElemento)
	{
		Node<T> actual = head;
		for(int i = 0; i < n; i++)
		{
			if(actual.getValue().equals(pElemento))
				return i;
			actual = actual.getNext();
		}
		
		return -1;
	}
	public T getPrevious() {
		if(current==null&&n==0){
			throw new NoSuchElementException();			
		}
		else if(current==null&&n>0)
		{
			current=head;
		}
		current=current.getPrevious();
		return current.getValue();
	}
	public boolean isEmpty() {
		return (head==null)?true:false;
	}
	public void addAtEnd(T t) {
		if(isEmpty()){
			head=new Node<T>(null, null,t);
			last= head;
			n++;
		}	
		else{
			Node<T> nuevo = new Node<T>(last, null, t);
			last.setNext(nuevo);
			last=nuevo;
			n++;
		}
	}	
	public void addAtK(int k, T t) throws IndexOutOfBoundsException{
		if(k>n){
			throw new IndexOutOfBoundsException();
		}
		else if(k==0&&(head!=null)){
			Node<T> nuevo = new Node<T>(null, head, t);
			head.setPrevious(nuevo);
			head=nuevo;
			n++;
		}
		else if(k==0&&(head==null)){
			head = new Node<T>(null, null, t);
			last=head;;
			n++;
		}
		else if(k==(n)){
			Node<T> nuevo = new Node<T>(last, null, t);
			last.setNext(nuevo);
			last=nuevo;
			n++;
		}
		else {
			int l =0;
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}			
			Node<T> nuevo = new Node<T>(current.getPrevious(), current, t);
			current.getPrevious().setNext(nuevo);
			current.setPrevious(nuevo);
			current=head;
			n++;		
		}
	}	
	public T getCurrentElement() {
		if(current==null&&n==0){
			throw new NoSuchElementException();
		}
		else if(current==null&&n!=0){
			current=head;
			return current.getValue();
		}
		else {
		return current.getValue();
		}
	}	
	public void deleteAtK(int k) throws NoSuchElementException{
		if(k>n){
			throw new NoSuchElementException();
		}
		if(k==0&&head==null){
			throw new NoSuchElementException();
		}
		else if (k==0&&head!=null){
			head=head.getNext();
			if(head != null)
				head.setPrevious(null);
			n--;
		}
		else if(k==(n-1)){
			last= last.getPrevious();
			last.setNext(null);
			n--;
		}
		else{
			int l=0;
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}
			current.getPrevious().setNext(current.getNext());
			current.getNext().setPrevious(current.getPrevious());
			n--;
		}
	}
	public boolean exists(T pElemento)
	{
		Node<T> actual = head;
		for(int i = 0; i < n; i++)
		{
			if(actual.getValue().equals(pElemento))
				return true;
			actual = actual.getNext();
		}
		return false; 
	}
	public void delete(T t) {
		if(head.getValue().equals(t)){
			deleteAtK(0);
			return;
		}
		else if(last.getValue().equals(t)){
			last=last.getPrevious();
			last.setNext(null);
			n--;
			return;
		}
		int k=0;
		current=head;
		while(current.hasNext()){
			k++;
			current=current.getNext();
			if(current.getValue().equals(t)){
				break;
			}
		}
		deleteAtK(k);
	}	

	@SuppressWarnings("unchecked")
	public Iterator<T> iterator() throws NoSuchElementException{
		if(head==null){
			T[] elements =(T[]) new Object[0];
			return new model.data_structures.Iterator(elements);
		}	
		T[] elements =(T[]) new Object[n];
		current=head;
		elements[0]=current.getValue();
		int l =0;		
		while(l<n){
			elements[l]= current.getValue();
			current= current.getNext();
			l++;		
		}
		return new model.data_structures.Iterator(elements);
		
	}
	public void add(T t) {
		addAtK(0, t);		
	}
	public T getElement(int k)throws NoSuchElementException {
		if(isEmpty()||k>=n){
			throw new NoSuchElementException();
		}		
		T elemento= null; int l=0;
		if(k==0){
			elemento= head.getValue();
		}
		else if(k==(n-1)){
			elemento=last.getValue();
		}
		else{
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}
			elemento=current.getValue();
		}
		return elemento;
	}
	@Override
	public String toString() {		
		String doubleLinkedList="[";
		T[] elements =(T[]) new Object[n];
		current=head;
		doubleLinkedList+=" " +current.getValue().toString() +" , ";
		int l =1;		
		while(l<n){
			current= current.getNext();
			if(l==n-1){
				doubleLinkedList+=" " +current.getValue().toString();
			}
			else{
			doubleLinkedList+=" " +current.getValue().toString() +" , ";			
			}			
			l++;
		}
		return doubleLinkedList+=" ]";		
	}
	
	public DoubleLinkedList<T> clone(){
		DoubleLinkedList<T> clone = new DoubleLinkedList<T>();
		current = head;
		for (int i = 0; i < (n-1); i++) {		
			clone.addAtEnd(current.getValue());
			current= current.getNext();
		}
		return clone;		
	}
	
	public T getLast()
	{
		return last.getValue();
	}
	
	//FIXME
	
	public void addAll(IList<T> otherList ){
		model.data_structures.Iterator<T> iter = (model.data_structures.Iterator<T>) otherList.iterator();
		while(iter.hasNext()){
			this.addAtEnd(iter.next());
		}		
	}
	
	@Override
	public boolean contains(T t) {
		// TODO Auto-generated method stub
		Node<T> actual = head;
		while(actual != null)
		{
			if(actual.getValue().equals(t))
				return true;
			actual = actual.getNext();
		}
		return false;
	}	
}