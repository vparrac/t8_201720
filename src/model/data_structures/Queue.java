package model.data_structures;

import java.util.NoSuchElementException;
public class Queue <T extends Comparable<T>> implements IQueue<T>{	
	private Node<T> head;
	private int size;
	private Node<T> last;
	/**
	 * M�todo constructor de clase.
	 */
	public Queue(){
		size = 0;
		head = null;
		last=null;
	}	
	/**
	 * Agrega un nuevo elemento a la pila
	 * @param pNuevo
	 * @throws NoSuchElementException
	 */
	public void enqueue(T pNuevo) throws NoSuchElementException{
		if(pNuevo==null){
			throw new NoSuchElementException();
		}
		if(size > 0){
			last.setNext(new Node<T>(last, null, pNuevo));
			last=last.getNext();
		}
		else
		{
			head = new Node<T>(null, null, pNuevo);
			last = head;
		}
			
		
		size++;		
	}
	/**
	 * Retorna el primer elemento y lo elimina de la pila.
	 * @return
	 */
	public T dequeue() throws NoSuchElementException{
		Node<T> aux = head;
		if(head==null||last==null)			
			throw new NoSuchElementException();
		if(size==1)
		{
			head=null;
			last = head;
		}
		else if(size > 1){		
			head = head.getNext();
			head.setPrevious(null);	
		}
		
		size--;
		return aux.getValue();
	}
	
	@Override
	public int size()
	{
		return size;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (size == 0)? true : false;
	}
	
	public String toString()
	{
		String toString = "[";
		
		Node<T> actual = head;
		while(actual != null)
		{
			if(!actual.equals(head))
				toString += ",";
			toString += actual.getValue().toString();
			actual = actual.getNext();
		}
		
		return toString + "]";
	}
}
