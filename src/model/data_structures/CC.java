package model.data_structures;

public class CC<K extends Comparable<K>,V>{
	
	private K[] vertices;
	private int id[];
	private int nConnectedComponents;
	
	private DFS<K,V> dfs;
	private DirectedGraph<K,V> grafo;
	
	public CC(DirectedGraph<K,V> grafo)
	{
		this.grafo = grafo;
		
		vertices = (K[]) new Comparable[grafo.v()];
		id = new int[grafo.v()];
		IList<Vertice<K,V>> verticesList = grafo.getVertices().list();
		
		for(int i = 0; i < vertices.length; i++)
		{
			vertices[i] = verticesList.getElement(i).getKey();
			id[i] = -1;
		}
//		print();
	}
	
	public void generateConnectedComponents()
	{
		dfs = new DFS<K,V>(grafo.reverse(), grafo.getVertice1());
		Stack<K> postOrdenInverso = dfs.postOrdenInverso();
		
		int i = 0;
		while(postOrdenInverso.seeNext() != null)
		{
			K verticeActual = postOrdenInverso.pop();
			if(id[buscarPos(verticeActual)] == -1)
			{
				dfs = new DFS<K,V>(grafo, verticeActual);
				dfs.generatePaths();
				Stack<K> conectados = dfs.getPostOrden();
				while(conectados.seeNext() != null)
				{
//					print();
					K vConectadoActual = conectados.pop();
					int posi = buscarPos(vConectadoActual);
					if(id[posi] == -1)
						id[posi] = i;
				}
				i++;
			}
			
		}
		nConnectedComponents = i;
	}
	
	public int nConnectedComponents()
	{
		return nConnectedComponents;
	}
	
	private int buscarPos(K key)
	{
		for(int i = 0; i < vertices.length; i++)
			if(vertices[i].equals(key))
				return i;
		return -1;//posible error.
	}
	
	private void print()
	{
		System.out.println("Components:");
		for(int i = 0; i < vertices.length; i++)
			System.out.println("|" + vertices[i] + "|" + id[i] + "|");
	}
	
	public IList<IList<K>> getConnectedComponents()
	{
		IList<IList<K>> connectedComponents = new DoubleLinkedList<IList<K>>();
		for(int i = 0; i < nConnectedComponents; i++)
		{
			IList<K> componenteActual = new DoubleLinkedList<K>();
			for(int j = 0; j < vertices.length; j++)
			{
				if(id[j] == i)
					componenteActual.addAtEnd(vertices[j]);
			}
			connectedComponents.addAtEnd(componenteActual);
		}
		
		return connectedComponents;
	}
}
