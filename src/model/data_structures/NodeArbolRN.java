package model.data_structures;

public class NodeArbolRN<K extends Comparable<K>,V>{
	
	
	// -------------------------------------------------------------
    // Atributos
    // -------------------------------------------------------------
	private K llave;
	private V valor;
	
	//Color del �rbol. true si es rojo, false si es negro.
	private boolean color;
	
	private NodeArbolRN<K,V> hijoIzquierdo;
	private NodeArbolRN<K,V> hijoDerecho;
	
	
	// -------------------------------------------------------------
    // Constructores
    // -------------------------------------------------------------
	public NodeArbolRN(K pLlave, V pValor){
		
		llave = pLlave;
		valor = pValor;
		color = true;
		
		hijoIzquierdo = null;
		hijoDerecho = null;
	}

	// -------------------------------------------------------------
    // M�todos
    // -------------------------------------------------------------
	public void setThis(K pLlave, V pValor, boolean pColor)
	{
		llave = pLlave;
		valor = pValor;
		color = pColor;
	}
	
	public K getLlave(){
		return llave;
	}
	
	public void setLlave(K pLlave){
		llave = pLlave;
	}
	
	public V getValor(){
		return valor;
	}
	
	public void setValor(V pValor){
		valor = pValor;
	}
	
	public boolean getColor(){
		return color;
	}
	
	public void setColor(boolean pColor){
		color = pColor;
	}
	
	public NodeArbolRN<K,V> getHijoIzquierdo(){
		return hijoIzquierdo;
	}
	
	public void setHijoIzquierdo(NodeArbolRN<K,V> pHijoIzquierdo){
		hijoIzquierdo = pHijoIzquierdo;
	}
	
	public NodeArbolRN<K,V> getHijoDerecho(){
		return hijoDerecho;
	}
	
	public void setHijoDerecho(NodeArbolRN<K,V> pHijoDerecho){
		hijoDerecho = pHijoDerecho;
	}
	
	/**
	 * Intercambia el color rojo de los hijos por el color negro del nodo actual.
	 */
	public boolean interCambiarColorConHijos(){
		if(hijoIzquierdo == null || hijoDerecho == null)
			return false;
		if(!hijoIzquierdo.getColor() || !hijoDerecho.getColor())
			return false; 
		
		hijoIzquierdo.setColor(false);
		hijoDerecho.setColor(false);
		color = true;
		
		return true;
	}
	
	/**
	 * Rota el hijoDerecho junto con este nodo como dando una vuelta hacia la izquierda.
	 */
	public boolean rotateLeft(){
		
		if(hijoDerecho == null)
			return false;
		if(!hijoDerecho.getColor())
			return false; 
		
		
		NodeArbolRN<K,V> este = new NodeArbolRN<K,V>(llave, valor);
		este.setHijoIzquierdo(hijoIzquierdo);
		este.setHijoDerecho(hijoDerecho.getHijoIzquierdo());
		este.setColor(hijoDerecho.getColor());
		
		valor = hijoDerecho.getValor();
		llave = hijoDerecho.getLlave();
		
		
		hijoDerecho = hijoDerecho.getHijoDerecho();
		hijoIzquierdo = este;
		
		return true;
	}
	
	
	/**
	 * Corrige el caso en el que el nodo actual tiene una secuencia de dos hijos izquierdos de color rojo.
	 */
	public boolean posicionarDosIzquierdosRojos(){
		if(hijoIzquierdo == null || hijoIzquierdo.getHijoIzquierdo()==null)
			return false;
		if(!hijoIzquierdo.getColor() || !hijoIzquierdo.getHijoIzquierdo().getColor())
			return false;
		
		NodeArbolRN<K,V> este = new NodeArbolRN<K,V>(llave, valor);
		este.setHijoIzquierdo(hijoIzquierdo.getHijoDerecho());
		este.setHijoDerecho(hijoDerecho);
		este.setColor(hijoIzquierdo.getColor());
		
		valor = hijoIzquierdo.getValor();
		llave = hijoIzquierdo.getLlave();
		
		hijoIzquierdo = hijoIzquierdo.getHijoIzquierdo();
		hijoDerecho = este;
		
		interCambiarColorConHijos();
		
		return true;
	}
	
	
	public K min()
	{
		if(hijoIzquierdo != null)
			return hijoIzquierdo.min();
		return llave;
	}
	
	public K max()
	{
		if(hijoDerecho != null)
			return hijoDerecho.max();
		return llave;
	}
	
	public void valuesInRange(IList<V> values, K min, K max)
	{
		if(llave.compareTo(min) > 0)
			if(hijoIzquierdo != null)
				hijoIzquierdo.valuesInRange(values, min, max);
		
		if(llave.compareTo(min) >= 0 && llave.compareTo(max) <= 0)
			values.addAtEnd(valor);
		
		if(llave.compareTo(max) < 0)
			if(hijoDerecho != null)
				hijoDerecho.valuesInRange(values, min, max);
	}
	
	public void keysInRange(IList<K> keys, K min, K max)
	{
		if(llave.compareTo(min) > 0)
			if(hijoIzquierdo != null)
				hijoIzquierdo.keysInRange(keys, min, max);
		
		if(llave.compareTo(min) >= 0 && llave.compareTo(max) <= 0)
			keys.addAtEnd(llave);
		
		if(llave.compareTo(max) < 0)
			if(hijoDerecho != null)
				hijoDerecho.keysInRange(keys, min, max);
	}
	
	public String toString()
	{
		return "<"+valor.toString() + " : " + llave.toString() + ">";
	}
}