package model.data_structures;

import java.util.Iterator;

/******************************************************************************
 *  Compilation:  javac LinearProbingHashST.java
 *  Execution:    java LinearProbingHashST < input.txt
 *  Dependencies: StdIn.java StdOut.java
 *  Data files:   http://algs4.cs.princeton.edu/34hash/tinyST.txt
 *  
 *  Symbol-table implementation with linear-probing hash table.
 *
 ******************************************************************************/
/**
 *  The {@code LinearProbingHashST} class represents a symbol table of generic
 *  key-value pairs.
 *  It supports the usual <em>put</em>, <em>get</em>, <em>contains</em>,
 *  <em>delete</em>, <em>size</em>, and <em>is-empty</em> methods.
 *  It also provides a <em>keys</em> method for iterating over all of the keys.
 *  A symbol table implements the <em>associative array</em> abstraction:
 *  when associating a value with a key that is already in the symbol table,
 *  the convention is to replace the old value with the new value.
 *  Unlike {@link java.util.Map}, this class uses the convention that
 *  values cannot be {@code null}�setting the
 *  value associated with a key to {@code null} is equivalent to deleting the key
 *  from the symbol table.
 *  <p>
 *  This implementation uses a linear probing hash table. It requires that
 *  the key type overrides the {@code equals()} and {@code hashCode()} methods.
 *  The expected time per <em>put</em>, <em>contains</em>, or <em>remove</em>
 *  operation is constant, subject to the uniform hashing assumption.
 *  The <em>size</em>, and <em>is-empty</em> operations take constant time.
 *  Construction takes constant time.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/34hash">Section 3.4</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *  For other implementations, see {@link ST}, {@link BinarySearchST},
 *  {@link SequentialSearchST}, {@link BST}, {@link RedBlackBST}, and
 *  {@link SeparateChainingHashST},
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class LinearProbingHashST<K, V> implements Iterable<K>{
	private static final int CAPACITY = 60;
	private int n;           // number of key-value pairs in the symbol table
	private int m;           // size of linear probing table
	private K[] keys;      // the keys
	private V[] vals;    // the values

	//Constructor 1
	public LinearProbingHashST() {
		this(CAPACITY);
	}

	//Constructor 2
	public LinearProbingHashST(int capacity) {
		m = capacity;
		n = 0;
		keys = (K[])   new Object[m];
		vals = (V[]) new Object[m];
	}

	//size
	public int size() {
		return n;
	}
	/**
	 * Returns true if this symbol table is empty.
	 * @return {@code true} if this symbol table is empty;
	 *         {@code false} otherwise
	 */
	public boolean isEmpty() {
		return size() == 0;
	}
	/**
	 * Returns true if this symbol table contains the specified key.
	 * @param  key the key
	 * @return {@code true} if this symbol table contains {@code key};
	 *         {@code false} otherwise
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public boolean contains(K key) {
		if (key == null) throw new IllegalArgumentException("argument to contains() is null");
		return get(key) != null;
	}
	//Valor absoluto
	private int hash(K key) {    
		return (key.hashCode() & 0x7fffffff) % m;
	}

	// Resizes the hash table to the given capacity by re-hashing all of the keys
	private void resize(int capacity) {
		LinearProbingHashST<K, V> temp = new LinearProbingHashST<K, V>(primoCercano(capacity));
		for (int i = 0; i < m; i++) {
			if (keys[i] != null) {
				temp.put(keys[i], vals[i]);
			}
		}
		keys = temp.keys;
		vals = temp.vals;
		m    = temp.m;
	}

	//Iteraci�n para encontrar el primo m�s cercano
	private int primoCercano(int m){
		boolean encontro=false;
		int primo=m;
		//Primo derecha
		while(!encontro){
			primo++;
			if(isPrime(primo)){
				encontro=true;
			}
		}
		return primo;    	
	}    
	private boolean isPrime(int m){
		boolean prime=true;
		int n = (int) Math.ceil(Math.sqrt(m));
		for (int i = 2; i <= n&&prime; i++) {
			boolean a = m%i==0;
			if(m%i==0){
				prime=false;
			}
		}
		return prime;
	}

	/**
	 * Inserts the specified key-value pair into the symbol table, overwriting the old 
	 * value with the new value if the symbol table already contains the specified key.
	 * Deletes the specified key (and its associated value) from this symbol table
	 * if the specified value is {@code null}.
	 * @param  key the key
	 * @param  val the value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	//FIXME
	public void put(K key, V val) {
		if (key == null) throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			delete(key);
			return;
		}
		// double table size if 75% full
		if (n >= m*0.75) resize(2*m);
		int i;

		for (i = hash(key); keys[i] != null; i = (i + 1) % m) {
			if (keys[i].equals(key)) {
				vals[i] = val;
				return;
			}
		}
		keys[i] = key;
		vals[i] = val;
		n++;
	}

	/**
	 * Returns the value associated with the specified key.
	 * @param key the key
	 * @return the value associated with {@code key};
	 *         {@code null} if no such value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */

	public V get(K key) {
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
		for (int i = hash(key); keys[i] != null; i = (i + 1) % m)
			if (keys[i].equals(key))
				return vals[i];
		return null;
	}

	/**
	 * Removes the specified key and its associated value from this symbol table     
	 * (if the key is in this symbol table).    
	 *
	 * @param  key the key
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void delete(K key) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");
		if (!contains(key)) return;

		// find position i of key
		int i = hash(key);
		while (!key.equals(keys[i])) {
			i = (i + 1) % m;
		}

		// delete key and associated value
		keys[i] = null;
		vals[i] = null;

		// rehash all keys in same cluster
		i = (i + 1) % m;
		while (keys[i] != null) {
			// delete keys[i] an vals[i] and reinsert
			K   keyToRehash = keys[i];
			V valToRehash = vals[i];
			keys[i] = null;
			vals[i] = null;
			n--;
			put(keyToRehash, valToRehash);
			i = (i + 1) % m;
		}
		n--;
		// halves size of array if it's 12.5% full or less
		if (n > 0 && n <= m/8) resize(m/2);
		assert check();
	}
	//    /**
	//     * Returns all keys in this symbol table as an {@code Iterable}.
	//     * To iterate over all of the keys in the symbol table named {@code st},
	//     * use the foreach notation: {@code for (Key key : st.keys())}.
	//     *
	//     * @return all keys in this symbol table
	//     */
	//    
	//    
	//    public Iterable<Key> keys() {
	//        Queue<Key> queue = new Queue<Key>();
	//        for (int i = 0; i < m; i++)
	//            if (keys[i] != null) queue.enqueue(keys[i]);
	//        return queue;
	//    }



	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator<K> iterator() {			
		V[] elements =(V[]) new Object[n];
		int contador=0;
		for (int i = 0; i < m; i++){
			if (keys[i] != null){ 
				elements[contador]= (V) keys[i];
				contador++;
			}
		}		
		return new model.data_structures.Iterator(elements);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object[] lista() {			
		V[] elements =(V[]) new Object[n];
		int contador=0;
		for (int i = 0; i < m; i++){
			if (keys[i] != null){ 
				elements[contador]= (V) keys[i];
				contador++;
			}
		}		
		return elements;
	}
		
	
// integrity check - don't check after each put() because
// integrity not maintained during a delete()

public DoubleLinkedList<V> list(){
	DoubleLinkedList<V> list = new DoubleLinkedList<V>();
	for (int i = 0; i < m; i++)
		if (vals[i] != null) list.addAtEnd((vals[i]));
	return list;
}
private boolean check() {
	// check that hash table is at most 50% full
	if (m < 2*n) {
		System.err.println("Hash table size m = " + m + "; array size n = " + n);
		return false;
	}

	// check that each key in table can be found by get()
	for (int i = 0; i < m; i++) {
		if (keys[i] == null) continue;
		else if (get(keys[i]) != vals[i]) {
			System.err.println("get[" + keys[i] + "] = " + get(keys[i]) + "; vals[i] = " + vals[i]);
			return false;
		}
	}
	return true;
}
/**
 * Unit tests the {@code LinearProbingHashST} data type.
 *
 * @param args the command-line arguments
 */

public static void main(String[] args) { 
	LinearProbingHashST<Integer, Integer> st = new LinearProbingHashST<Integer, Integer>();
	Integer uno = 1;
	Integer dos = 2;
	Integer tres = 3;
	Integer cuatro = 4;

	st.put(uno.hashCode(),uno);
	st.put(dos.hashCode(), dos);
	st.put(tres.hashCode(), tres);
	st.put(cuatro.hashCode(), cuatro);
	System.out.println(st.get(uno.hashCode()));
	st.delete(dos.hashCode());
	System.out.println(st.size());

	long time = System.currentTimeMillis();
	//For
	for (int i = 0; i < 3000000; i++) {
		Integer a = i;
		st.put(a.hashCode(), a);
	}
	long time1 = System.currentTimeMillis();


	time = System.currentTimeMillis();
	System.out.println(st.get(new Integer(7685).hashCode()));
	time1 = System.currentTimeMillis();
	System.err.println(time1-time);   

}
}