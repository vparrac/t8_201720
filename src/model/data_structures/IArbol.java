package model.data_structures;


public interface IArbol<K extends Comparable<K>,V >{
	
	/**
	 * Retorna el tamanio del �rbol.
	 * @return
	 */
	public int size();
	
	/**
	 * Retorna un valor booleano comunicando si el �rbol est� vac�o.
	 * @return
	 */
	public boolean isEmpty();
	
	/**
	 * Retorna la altura del �rbol.
	 * @return
	 */
	public int height();
	
	/**
	 * Da el elemento guardado en el �rbol con la llave dada.
	 * @param pValor
	 * @return
	 */
	public V get(K pLlave);
	
	/**
	 * Agrega un nuevo elemento al �rbol.
	 * @param pLlave
	 * @param pValor
	 */
	public void add(K pLlave, V pValor);
	
	/**
	 * Retorna un arreglo con los elementos del �rbol organizados en un recorrido en inorden.
	 * @return
	 */
	public model.data_structures.Iterator<V> recorridoInorden();
	
	/**
	 * Retorna un arreglo con los elementos del �rbol organizados en un recorrido en preOrden.
	 * @return
	 */
	public IQueue<V> recorridoPreOrden();
	
	/**
	 * Retorna un arreglo con los elementos del �rbol organizados en un recorrido en postOrden.
	 * @return
	 */
	public IQueue<V> recorridoPostOrden();
	
	/**
	 * Retorna un arreglo con los elementos del �rbol organizados en un recorrido por niveles.
	 * @return
	 */
	public IQueue<V> recorridoPorNiveles();
	
	public model.data_structures.Iterator<K> keys();
	
	public model.data_structures.Iterator<V> valuesInRange(K min, K max);
	
	public model.data_structures.Iterator<K> keysInRange(K min, K max);
	
	public K min();
	
	public K max();
	
	public void deleteMin();
	
	public void deleteMax();
	
	public void delete(K key);

}