package model.data_structures;
import java.io.Serializable;
import java.util.NoSuchElementException;
public class DoubleList<K,V> implements Comparable<DoubleList<K,V>>, Serializable {
	private static final long serialVersionUID = 1L;
	private int n;
	private DoubleNode<K,V> head;
	private DoubleNode<K,V> last;
	private DoubleNode<K,V> current;
	public DoubleList() {		
		n = 0;	
		head=null;
		last=null;
		current=null;
	}
	public int size() {return n;}
	
	
	public int getPosicionElemento(K pElemento)
	{
		DoubleNode<K,V> actual = head;
		for(int i = 0; i < n; i++)
		{
			if(actual.getKey().equals(pElemento))
				return i;
			actual = actual.getNext();
		}
		
		return -1;
	}
	
	public boolean isEmpty() {
		return (head==null)?true:false;
	}
	
	public boolean exists(K key)
	{
		DoubleNode<K,V> actual = head;
		for(int i = 0; i < n; i++)
		{
			if(actual.getKey().equals(key))
				return true;
			
			actual = actual.getNext();
		}
		return false;
	}
	
	public boolean addAtEnd(K key, V value) {
		if(exists(key))
			return false;
		if(isEmpty()){
			head=new DoubleNode<K,V>(null, null, key, value);
			last= head;
			n++;
		}	
		else{
			DoubleNode<K,V> nuevo = new DoubleNode<K,V>(last, null, key, value);
			last.setNext(nuevo);
			last=nuevo;
			n++;
		}
		return true;
	}	
	public V get(K key)
	{
		DoubleNode<K,V> actual = head;
		for(int i = 0; i < n; i++)
		{
			if(actual.getKey().equals(key))
				return actual.getValue();
			actual = actual.getNext();
		}
		return null;
	}
	
	public boolean addAtK(int k, K key, V value) throws IndexOutOfBoundsException{
		if(exists(key))
			return false;
		if(k>n){
			throw new IndexOutOfBoundsException();
		}
		else if(k==0&&(head!=null)){
			DoubleNode<K,V> nuevo = new DoubleNode<K,V>(null, head, key, value);
			head.setPrevious(nuevo);
			head=nuevo;
			n++;
		}
		else if(k==0&&(head==null)){
			head = new DoubleNode<K,V>(null, null, key, value);
			last=head;;
			n++;
		}
		else if(k==(n)){
			DoubleNode<K,V> nuevo = new DoubleNode<K,V>(last, null, key, value);
			last.setNext(nuevo);
			last=nuevo;
			n++;
		}
		else {
			int l =0;
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}			
			DoubleNode<K,V> nuevo = new DoubleNode<K,V>(current.getPrevious(), current, key, value);
			current.getPrevious().setNext(nuevo);
			current.setPrevious(nuevo);
			current=head;
			n++;		
		}
		return true;
	}
	
	public V getCurrentElement() {
		if(current==null&&n==0){
			throw new NoSuchElementException();
		}
		else if(current==null&&n!=0){
			current=head;
			return current.getValue();
		}
		else {
		return current.getValue();
		}
	}	
	public void deleteAtK(int k) throws NoSuchElementException{
		if(k>n){
			throw new NoSuchElementException();
		}
		if(k==0&&head==null){
			throw new NoSuchElementException();
		}
		else if (k==0&&head!=null){
			head=head.getNext();
			if(head != null)
				head.setPrevious(null);
			n--;
		}
		else if(k==(n-1)){
			last= last.getPrevious();
			last.setNext(null);
			n--;
		}

		else{
			int l=0;
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}
			current.getPrevious().setNext(current.getNext());
			current.getNext().setPrevious(current.getPrevious());
			n--;
		}
	}
	
	public boolean delete(K key) {
		if(head.getKey().equals(key)){
			deleteAtK(0);
			return true;
		}
		else if(last.getKey().equals(key)){
			last=last.getPrevious();
			last.setNext(null);
			n--;
			return true;
		}
		boolean seEncontro = false;
		int k=0;
		current=head;
		while(current.hasNext()){
			k++;
			current=current.getNext();
			if(current.getKey().equals(key)){
				seEncontro = true;
				break;
			}
		}
		deleteAtK(k);
		return seEncontro;
	}	

	
	public boolean add(K key, V value) {
		if(addAtK(0, key, value))
			return true;
		
		return false;
	}
	public V getElement(int k)throws NoSuchElementException {
		if(isEmpty()||k>=n){
			throw new NoSuchElementException();
		}		
		V elemento= null; int l=0;
		if(k==0){
			elemento= head.getValue();
		}
		else if(k==(n-1)){
			elemento=last.getValue();
		}
		else{
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}
			elemento=current.getValue();
		}
		return elemento;
	}
	
	public K getKey(int k)
	{
		if(isEmpty()||k>=n){
			throw new NoSuchElementException();
		}		
		K key= null; int l=0;
		if(k==0){
			key= head.getKey();
		}
		else if(k==(n-1)){
			key=last.getKey();
		}
		else{
			current=head;
			while(l<k){
				current=current.getNext();
				l++;
			}
			key=current.getKey();
		}
		return key;
	}
	
	
	public DoubleList<K,V> clone(){
		DoubleList<K,V> clone = new DoubleList<K,V>();
		current = head;
		for (int i = 0; i < (n-1); i++) {		
			clone.addAtEnd(current.getKey(), current.getValue());
			current= current.getNext();
		}
		return clone;		
	}
	//No se ha implementado.
	@Override
	public int compareTo(DoubleList<K, V> arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString()
	{
		String toString = "[ ";
		DoubleNode<K,V> actual = head;
		while(actual.hasNext())
		{
			toString += " <"+actual.getKey()+":"+actual.getValue()+">";
			actual = actual.getNext();
		}
		
		return toString + " <"+actual.getKey()+":"+actual.getValue()+">" + " ]";
	}
}