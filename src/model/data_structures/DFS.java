package model.data_structures;

public class DFS<K extends Comparable<K>,V>{
	
	private K[] vertices;
	private boolean[] marcados;
	private K[] edgeFrom;
	private DirectedGraph<K,V> grafo;
	private K origin;
	private Stack<K> postOrden;
	
	public DFS(DirectedGraph<K,V> grafo, K origin)
	{
		this.grafo = grafo;
		this.origin = origin;
		
		IList<Vertice<K,V>> verticesList = grafo.getVertices().list();
		vertices = (K[]) new Comparable[verticesList.size()];
		marcados = new boolean[verticesList.size()];
		edgeFrom = (K[]) new Comparable[verticesList.size()];
		int i = 0;
		for(Vertice<K,V> actual : verticesList)
		{
			vertices[i] = actual.getKey();
			marcados[i] = false;
			edgeFrom[i] = null;
			i++;
		}
		
		postOrden = new Stack<K>();
	}
	
	public boolean hasPathTo(K key)
	{
		int posi = buscarPos(key);
		return (posi == -1)? false : (marcados[posi])? true : false;
		
	}
	
	public void generatePaths()
	{
		int posi = buscarPos(origin);
		marcados[posi] = true;
		//M�todo usado para test.
		//imprimir();
		generatePath(origin, new Stack<Edge<K>>());
	}
	
	private void generatePath(K actual, Stack<Edge<K>> arcosRecursivos)
	{
		PriorityQueue<Edge<K>> arcosActual = grafo.getEdgesAsPriorityQueue(actual);
		if(arcosActual == null || arcosActual.size() == 0)
		{
			postOrden.push(actual);
			return;
		}
			
		while(arcosActual.size() > 0)
			arcosRecursivos.push(arcosActual.dequeue());
		
		Edge<K> arcoActual;
		
		while(arcosRecursivos.seeNext() != null && arcosRecursivos.seeNext().getSource().equals(actual))
		{
			arcoActual = arcosRecursivos.pop();
			int posi = buscarPos(arcoActual.getDest());
			if(!marcados[posi])
			{
				marcados[posi] = true;
				edgeFrom[posi] = actual;
				//M�todo usado para test.
				//imprimir();
				generatePath(arcoActual.getDest(), arcosRecursivos);
			}
			
		}
		
		postOrden.push(actual);
	}
	
	public IList<K> pathTo(K key)
	{
		int posi = buscarPos(key);
		if(posi == -1)
			return null;
		
		IList<K> path = new DoubleLinkedList<K>();
		
		path.add(key);
		if(hasPathTo(key))
			pathTo(key, path);
		return path;
		
	}
	
	private void pathTo(K actual, IList<K> path)
	{
		int posi = buscarPos(actual);
		K source = edgeFrom[posi];
		path.add(source);
		if(source.equals(origin))
			return;
		pathTo(source, path);
	}
	
	private int buscarPos(K key)
	{
		for(int i = 0; i < vertices.length; i++)
			if(vertices[i].equals(key))
				return i;
		return -1;//posible error.
	}
	
	public Stack<K> getPostOrden()
	{
		return postOrden;
	}
	
	public Stack<K> postOrdenInverso()
	{
		int posSeparada = posSeparada();
		while(posSeparada != -1)
		{
			origin = vertices[posSeparada];
			generatePaths();
			
			posSeparada = posSeparada();
			
		}
		return postOrden;
	}
	
	private int posSeparada()
	{
		for(int i = 0; i < marcados.length; i++)
			if(!marcados[i])
				return i;
		return -1;
	}
	
	public IList<K> getSeparadas()
	{
		IList<K> separadas = new DoubleLinkedList<K>();
		for(int i = 0; i < marcados.length; i++)
			if(!marcados[i])
				separadas.addAtEnd(vertices[i]);
		return separadas;
	}

	public void imprimir()
	{
		System.out.println("++++++++++++++++++++++++++");
		for(int i = 0; i < vertices.length; i++)
			if(vertices[i] != null)
				System.out.println("|"+vertices[i].toString() + "|" + marcados[i] + "|" + edgeFrom[i] + "|");
		
	}
	
}
