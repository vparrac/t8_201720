package model.data_structures;

public class Edge<K extends Comparable<K>> implements Comparable<Edge<K>>{	
	private K source;
	private K dest;
	private double weight;
	private boolean compararPorWeight;	
	public Edge(K source, K dest, double weight){
		this.source = source;
		this.dest = dest;
		this.weight = weight;
		compararPorWeight = false;
	}	
	public K getSource()
	{
		return source;
	}
	
	public K getDest()
	{
		return dest;
	}
	
	public double getWeight()
	{
		return weight;
	}
	
	public void compararPorWeight()
	{
		compararPorWeight = true;
	}

	@Override
	public int compareTo(Edge<K> edge) {
		// TODO Auto-generated method stub
		if(compararPorWeight)
			return (int) (this.weight-edge.getWeight());
		return (source.compareTo(edge.getSource()) + dest.compareTo(edge.getDest()));
	}
	
	public String toString()
	{
		return "<" + source.toString() + "> -|" + weight + "|-> <" + dest.toString() + ">"; 
	}
	
	@Override
	public boolean equals(Object edge)
	{
		Edge<K> arco = (Edge<K>) edge;
		return compareTo(arco) == 0? true : false;
	}
	
}
