package model.data_structures;

public class SeparateChaining <K extends Comparable<K>, V>{
	private DoubleList<K,V>[] tabla;
	private int n;	//Total elementos.
	private int k;	//Total casillas de tabla.
	private double carga;
	private double cargaMaxima;
	
	public SeparateChaining(int kValue, double cargaFactor)
	{
		
		n = 0; 
		cargaMaxima = cargaFactor;
		
		instanciarVariables(kValue);
		
	}
	
	public SeparateChaining(int kValue)
	{
		
		n = 0; 
		cargaMaxima = 20;
		
		instanciarVariables(kValue);
		
	}
	
	public boolean isEmpty()
	{
		return (n==0)? true : false;
	}
	
	public int size()
	{
		return n;
	}
	
	public int getK()
	{
		return k;
	}
	
	//Falta lanzar excepciones: 1. Si key o value == null, 2. Si elemento repetido.
	public void put(K key, V value)
	{
		if(key == null || value == null)
			return;
		
		verificarCarga();
		
		int hashValue = hash(key);
		if(tabla[hashValue].add(key, value))
			n++;
		carga = (double) n/k;
		
		//testAdd
		/**
		System.out.println("같같같같같같같");
		System.out.println("Agregado: " + key.toString());
		System.out.println("size: " + n);
		System.out.println("K: " + k);
		System.out.println("Carga: " + carga);
		*/
	}
	
	public V get(K key)
	{
		if(key == null)
			return null;
		
		int posi = hash(key);
		
		return tabla[posi].get(key);
	}
	
	public void delete(K key)
	{
		int posi = hash(key);
		if(tabla[posi].delete(key))
			n--;
		carga = (double) n/k;
	}
	
	private void verificarCarga()
	{
		if(carga > cargaMaxima)
		{
			//System.err.println("++++++++Se verific�.+++++++++");
			DoubleList<K,V>[] actual = tabla;
			int nuevoKPrimo = primoCercano(k);
			tabla =  new DoubleList[nuevoKPrimo];
			instanciarVariables(nuevoKPrimo);
			
			K key; V value;
			for(int i = 0; i < actual.length; i++)
			{
				for(int j = 0; j < actual[i].size(); j++)
				{
					key = actual[i].getKey(j);
					value = actual[i].getElement(j);
					put(key, value);
				}
			}
			//System.err.println("+++++++++++++++++++++++++++++");
		}
	}
	
	public DoubleList<K, V> getElements()
	{
		DoubleList<K,V> retornada = new DoubleList<K,V>();
		
		for(DoubleList<K,V> actual : tabla)
		{
			for(int i = 0; i < actual.size(); i++)
				retornada.addAtEnd(actual.getKey(i), actual.getElement(i));
		}
		
		return retornada;
	}
	
	private void instanciarTabla(DoubleList<K,V>[] instanciada)
	{
		for(int i = 0; i < instanciada.length; i++)
			instanciada[i] = new DoubleList<K,V>();
	}
	
	private int hash(K key)
	{
		return Math.abs(key.hashCode()) % k;
	}
	
	private void instanciarVariables(int kValue)
	{
		n = 0;
		k = kValue;
		tabla = new DoubleList[kValue];
		instanciarTabla(tabla);
		carga = (double) n/k;
	}
	
	private int primoCercano(int m){
    	boolean encontro=false;
    	int primo=m;
    	//Primo derecha
    	while(!encontro){
    		primo++;
    		if(isPrime(primo)){
    			encontro=true;
    		}
    	}
    	return primo;    	
    }    
    private boolean isPrime(int m){
    	boolean prime=true;
    	int n = (int) Math.ceil(Math.sqrt(m));
    	for (int i = 2; i <= n&&prime; i++) {
    		boolean a = m%i==0;
			if(m%i==0){
				prime=false;
			}
		}
    	return prime;
    }
}