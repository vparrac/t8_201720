package model.data_structures;

public class Cycle<K extends Comparable<K>,V>{
	
	
	private DirectedGraph<K,V> grafo;
	private CC<K,V> cc;
	private IList<IList<K>> cycles;
	
	public Cycle(DirectedGraph<K,V> grafo) 
	{
		this.grafo = grafo;
		cc = new CC<K,V>(grafo);
		cycles = new DoubleLinkedList<IList<K>>();
	}
	
	public void sourceCycles()
	{
		cc.generateConnectedComponents();
		IList<IList<K>> connectedComponents = cc.getConnectedComponents();
		
		for(IList<K> actualConnectedComponent : connectedComponents)
		{
			if( actualConnectedComponent.size() > 1)
				cycles.addAtEnd(actualConnectedComponent);
		}
	}
	
	public boolean hasCycles()
	{
		return (!cycles.isEmpty())? true : false;
	}
	
	public IList<IList<K>> getCycles()
	{
		return cycles;
	}
	
	public IList<K> getCycle(K sourced)
	{
		for(IList<K> actualCycle : cycles)
			if(actualCycle.contains(sourced))
				return actualCycle;
		return null;
	}
	
	public int nCycles()
	{
		return cycles.size();
	}
	
}
