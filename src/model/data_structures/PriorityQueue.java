package model.data_structures;

public 
class PriorityQueue<T extends Comparable<T>> {
	
	private T[] heap;
	private int n;
	private boolean aMayor;
	
	public PriorityQueue(boolean pAMayor)
	{
		n = 0;
		heap = (T[]) new Comparable[4];
		aMayor = pAMayor;
	}
	
	public int size()
	{
		return n;
	}

	public void enqueue(T pNuevo)
	{
		n++;
		revisarSiHeapLleno();
 		heap[n] = pNuevo;
		intercambiarArriba(n, n/2);		
	}
	
	public T dequeue()
	{
		if(n == 0)
			return null;
		T dequeued = heap[1];
		
		heap[1] = heap[n];
		heap[n] = null;
		n--;
		intercambiarAbajo(1);
		
		
		return dequeued;
	}
	
	public IList<T> heapSort()
	{
		IList<T> elementos = new DoubleLinkedList<T>();
		int tamanio = n;
		for(int i = 0; i < tamanio; i++)
			elementos.addAtEnd(dequeue());
		
		
		return elementos;
	}
	
	public IList<T> heapSort(IList<T> desordenados, boolean pAMayor)
	{
		if(desordenados == null)
			return null;
		
		aMayor = pAMayor;
		heap = (T[]) new Comparable[desordenados.size()+1];
		n = heap.length-1;
		for(int i = 1; i <= n; i++){
			heap[i] = desordenados.getElement(i-1);
			
		}
		//Primera parte del heapSort -Ordenar �rbol por prioridad-.	
		prioritize(n);
		
		int nActual = n;
		//Ordena los elementos de forma inversa a la del orden por prioridad.+
		sort();
		
		IList<T> ordenados = new DoubleLinkedList<T>();
		for(int i = 1; i <= nActual; i++)
			ordenados.addAtEnd(heap[i]);
		return ordenados;
	}
	
	private void prioritize(int posi)
	{
		if(posi <= 1)
			return;
		int posMayor = posi;
		if(posi%2 == 1)
		{
			if(compararPorPrioridad(posi-1, posi))
				posMayor = posi-1;
		}
		
		if(compararPorPrioridad(posMayor, posMayor/2))
			intercambiarAbajo(posMayor/2);
		
		prioritize( (posMayor%2 == 0)? posMayor-1 : posMayor-2);
	}
	
	private void sort()
	{
		if(n == 1)
			return;
		
		//Intercambiar la cabeza con el �ltimo.
		T actual = heap[1];
		heap[1] = heap[n];
		heap[n] = actual;
		
		n--;
		
		intercambiarAbajo(1);
		sort();
		
	}
	
	private void intercambiarArriba(int posi, int padre)
	{
		if(posi <= 1)
			return;
		if(!compararPorPrioridad(posi, padre))
			return;
		intercambiarPosiciones(posi, padre);
		intercambiarArriba(padre, padre/2);
	}
	
	private void intercambiarAbajo(int posi)
	{
		if(posi*2>n)
			return;
		
		if(posi*2+1 > n)
		{
			if(compararPorPrioridad(posi*2, posi))
			{
				intercambiarPosiciones(posi, posi*2);
				posi *= 2;
			}
			else
				return;
				
			
		}
		else
		{
			int posHijoMayor = posi*2;
			if(compararPorPrioridad(posi*2+1, posi*2))
				posHijoMayor++;
			if(compararPorPrioridad(posHijoMayor, posi))
			{
				intercambiarPosiciones(posi, posHijoMayor);
				posi = posHijoMayor;
			}
			else
				return;
				
		}
		intercambiarAbajo(posi);
		
	}

	
	private void revisarSiHeapLleno()
	{
		
		if(n >= heap.length-1)
		{
			T[] heapNuevo = (T[]) new Comparable[n*2 - n/4];
			for(int i = 0; i < n; i++)
				heapNuevo[i] = heap[i];
			heap = heapNuevo;
		}
	}
	
	private void intercambiarPosiciones(int pos1, int pos2)
	{
		T actual = heap[pos1];
		heap[pos1] = heap[pos2];
		heap[pos2] = actual;
	}
	
	private boolean compararPorPrioridad(int posiElemento1, int posiElemento2)
	{
		T elemento1 = heap[posiElemento1], elemento2 = heap[posiElemento2];
		return (aMayor)? (elemento1.compareTo(elemento2) > 0? true : false) : (elemento1.compareTo(elemento2) < 0? true : false);
	}
}
