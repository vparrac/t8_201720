package model.data_structures;
public class DirectedGraph<K extends Comparable<K>,V>{
	private LinearProbingHashST<K , Vertice<K,V>> vertices;
	
	/**
	 * Jhonnathan, agregue esto para saber aqu� la lista de todos los edges
	 */
	private ArbolRojoNegro<Double, Edge<K>> edges;
	private int e;
	private int v;
	
	public DirectedGraph()
	{
		edges= new ArbolRojoNegro<>();
		vertices = new LinearProbingHashST<K, Vertice<K,V>>();
		e = 0; 
		v = 0;
	}
	
	
	public void addVertice(K key, V value){
		
		if(vertices.contains(key))
			vertices.get(key).setValue(value);
		
		else{
			vertices.put(key, new Vertice<K,V>(key, value));
			v++;
		}			
	}	
	public void addEdge(K source, K dest, double weight)
	{
		if(!vertices.contains(source) || !vertices.contains(dest))
			return;
		if(vertices.get(source).addEdge(new Edge<K>(source, dest, weight))){
			e++;
			edges.add(weight, new Edge<K>(source, dest, weight));
		}
	}	
	public V getValue(K key)
	{
		Vertice<K,V> sourced = vertices.get(key);
		return (sourced != null)? sourced.getValue() : null;
	}
	
	public boolean isEmpty()
	{
		return (v == 0)? true : false;
	}
	
	public int v()
	{
		return v;
	}
	
	public int e()
	{
		return e;
	}
	
	//Falta probar.
	public DirectedGraph<K,V> reverse()
	{
		DirectedGraph<K,V> reversed = new DirectedGraph<K,V>();
		IList<Vertice<K,V>> verticesList = vertices.list();
		for(Vertice<K,V> actual : verticesList)
		{
			reversed.addVertice(actual.getKey(), actual.getValue());
			IList<Edge<K>> arcosActual = actual.getEdges();
			
		}
		for(Vertice<K,V> actual : verticesList)
		{
			IList<Edge<K>> arcosActual = actual.getEdges();
			
			for(Edge<K> arcoActual : arcosActual)
			{
				reversed.addEdge(arcoActual.getDest(), arcoActual.getSource(), arcoActual.getWeight());
			}
				
		}
		
		return reversed;
	}
	
	public IList<K> adj(K v)
	{
		if(!vertices.contains(v))
			return null;
		IList<Edge<K>> arcosV = vertices.get(v).getEdges();
		IList<K> adjs = new DoubleLinkedList<K>();
		for(Edge<K> arcoActual : arcosV)
			adjs.addAtEnd(arcoActual.getDest());
		return adjs;
	}
	
	public IList<Edge<K>> getEdges(K source)
	{
		Vertice<K,V> sourced = vertices.get(source);
		return (sourced != null)? sourced.getEdges() : null;
	}
	
	public PriorityQueue<Edge<K>> getEdgesAsPriorityQueue(K source)
	{
		Vertice<K,V> sourced = vertices.get(source);
		return (sourced != null)? sourced.getEdgesAsBinaryHeap() : null;
	}
	
	public LinearProbingHashST<K, Vertice<K, V>> getVertices()
	{
		return vertices;
	}
	
	public void deleteVertice(K key)
	{
		Vertice<K,V> sourced = vertices.get(key);
		if(sourced == null)
			return;
		
		int degree = sourced.getDegree();
		e -= degree;
		vertices.delete(key);
		v--;
		
		revisarArcosInservibles(key);
	}
	
	public void deleteEdge(K source, K dest)
	{
		Vertice<K,V> sourced = vertices.get(source);
		if(sourced == null)
			return;
		
		if(sourced.deleteEdge(dest))
			e--;
	}
	
	public static void main(String[] args){
		
	}
	
	public K getVertice1(){
		return (!vertices.isEmpty())? vertices.list().getElement(0).getKey() : null;
	}
	
	private void revisarArcosInservibles(K dest)
	{
		IList<Vertice<K,V>> verticesList = vertices.list();
		for(Vertice<K,V> actual : verticesList)
		{
			if(actual.deleteEdge(dest))
				e--;
		}
	}
}
