package model.data_structures;

public class DoubleNode<K,V>{
	
	private K llave;
	private V valor;
	private DoubleNode<K,V> next;
	private DoubleNode<K,V> previous;
	
	public DoubleNode(DoubleNode<K,V> pPrevious, DoubleNode<K,V> pNext, K pLlave, V pValor)
	{
		llave = pLlave;
		valor = pValor;
		previous = pPrevious;
		next = pNext;
	}
	
	public K getKey()
	{
		return llave;
	}
	
	public V getValue()
	{
		return valor;
	}
	
	public void setNext(DoubleNode<K,V> siguiente)
	{
		next = siguiente;
	}
	
	public DoubleNode<K,V> getNext()
	{
		return next;
	}
	
	public void setPrevious(DoubleNode<K,V> anterior)
	{
		previous = anterior;
	}
	public DoubleNode<K,V> getPrevious()
	{
		return previous;
	}
	
	public boolean hasPrevious()
	{
		return (previous != null)? true : false;
	}
	
	public boolean hasNext()
	{
		return (next != null)? true : false;
	}

}