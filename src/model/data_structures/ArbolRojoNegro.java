package model.data_structures;


public class ArbolRojoNegro<K extends Comparable<K>, V  > implements IArbol<K,V>
{
	
	
	// -------------------------------------------------------------
    // Constantes
    // -------------------------------------------------------------
	
	private final static boolean ROJO=true;
	
	private final static boolean NEGRO=false;
	
	
	// -------------------------------------------------------------
    // Atributos
    // -------------------------------------------------------------
	
	private int size;
	//Se maneja la altura en base a lo que dice el libro de la clase -Algorithms-. Por tanto: si cabeza != null y hijosCabeza == null, tamanio = 0;
	private int altura;
	
	private NodeArbolRN<K,V> head;
	
	
	// -------------------------------------------------------------
    // Constructores
    // -------------------------------------------------------------
	/**
	 * M�todo constructor de la clase.
	 */
	public ArbolRojoNegro(){
		size = 0;
		altura = -1;
		head = null;
	}

	// -------------------------------------------------------------
    // M�todos
    // -------------------------------------------------------------
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0? true : false;
	}
	
	@Override
	public int height(){
		return altura;
	}

	@Override
	public V get(K pLlave) {
		// TODO Auto-generated method stub
		if(head==null)
			return null;
		
		return buscar(head, pLlave);
	}

	@Override
	public void add(K pLlave, V pValor) {
		// TODO Auto-generated method stub
		
		if(head==null){
			head = new NodeArbolRN<K,V>(pLlave, pValor);
			normalizarColorCabeza();
			size++;
			altura = 0;
			return;
		}
		
		agregar(pLlave, pValor, head, 0);
		normalizarColorCabeza();
	}

	 public DoubleLinkedList<V> recorridoInordenList() {
			// TODO Auto-generated method stub
			DoubleLinkedList<V> recorridoInorden = new DoubleLinkedList<V>();
			getInorden(recorridoInorden, head);
			
			return recorridoInorden;
		}
	@Override
    public model.data_structures.Iterator<V> recorridoInorden() {
		// TODO Auto-generated method stub
		DoubleLinkedList<V> recorridoInorden = new DoubleLinkedList<V>();
		getInorden(recorridoInorden, head);
		
		return (model.data_structures.Iterator<V>) recorridoInorden.iterator();
	}

	@Override
	public IQueue<V> recorridoPreOrden() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IQueue<V> recorridoPostOrden() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IQueue<V> recorridoPorNiveles() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	/**
	 * M�todo recursivo que busca el valor de el nodo que contenga la llave.
	 * @param actual
	 * @param pLlave
	 * @return valor del nodo encontrado, null en caso de que no est�.
	 */
	private V buscar(NodeArbolRN<K,V> actual, K pLlave){
		if(actual == null || pLlave ==null)
		{	
			return null;
		}
		
		int comp = pLlave.compareTo(actual.getLlave());
		
		if(comp==0)
		{	
			return actual.getValor();
		}
		else if(comp<0)
		{
			return buscar(actual.getHijoIzquierdo(), pLlave);
		}
		else
		{
			return buscar(actual.getHijoDerecho(), pLlave);		
		}
	}
	
	/**
	 * M�todo recursivo que agrega una nueva dupla al �rbol.
	 * En caso de que exista un elemento con la llave dada, se cambia el valor de la dupla.
	 * @param pLlave
	 * @param pValor
	 * @param pActual
	 * @param pAltura
	 */
	private void agregar(K pLlave, V pValor, NodeArbolRN<K,V> pActual, int pAltura){
		
		
		int comp = pLlave.compareTo(pActual.getLlave());	
		
		if(comp==0)
			pActual.setValor(pValor);
		
		else if(comp < 0){
			
			if(pActual.getHijoIzquierdo() == null){
				pActual.setHijoIzquierdo(new NodeArbolRN<K,V>(pLlave, pValor));
				size++;
				if(++pAltura > altura)
					altura++;
			}
			else
				agregar(pLlave, pValor, pActual.getHijoIzquierdo(), ++pAltura);
		}
			
		else{
			
			if(pActual.getHijoDerecho() == null){
				pActual.setHijoDerecho(new NodeArbolRN<K,V>(pLlave, pValor));
				size++;
				if(++pAltura > altura)
					altura++;
			}
			else
				agregar(pLlave, pValor, pActual.getHijoDerecho(), ++pAltura);
		}
			
		corregirPosiciones(pActual);
		
	}
	
	/**
	 * Corrige los posibles errores de posiciones que se pueden dar en un arbol rojo-negro.
	 * @param corregido
	 * @return true en caso de que se aplique rotateLeft o corregimiento de dos hijos izquierdos rojos, false en caso contrario.
	 */
	private boolean corregirPosiciones(NodeArbolRN<K,V> corregido){
		
		
		if(!corregido.interCambiarColorConHijos())
		{
			if(!corregido.rotateLeft())
				return corregido.posicionarDosIzquierdosRojos();
			else
				return true;
		}
		
		return false;
	}
		
	/**
	 * Actualiza el color de la cabeza a negro.
	 */
	private void normalizarColorCabeza(){
		head.setColor(NEGRO);
	}
	
	
	
	

	@Override
	public model.data_structures.Iterator<K> keys() {
		// TODO Auto-generated method stub
		DoubleLinkedList<K> recorridoInorden = new DoubleLinkedList<K>();
		getInOrdenKeys(recorridoInorden, head);
		
		return (model.data_structures.Iterator<K>) recorridoInorden.iterator();
	}

	@Override
	public model.data_structures.Iterator<V> valuesInRange(K min, K max) {
		// TODO Auto-generated method stub
		if(size == 0)
			return null;
		IList<V> values = new DoubleLinkedList<V>();
		head.valuesInRange(values, min, max);
		return (model.data_structures.Iterator<V>) values.iterator();
	}

	@Override
	public Iterator<K> keysInRange(K min, K max) {
		// TODO Auto-generated method stub
		if(size == 0)
			return null;
		IList<K> keys = new DoubleLinkedList<K>();
		head.keysInRange(keys, min, max);
		return (model.data_structures.Iterator<K>) keys.iterator();
	}

	@Override
	public K min() {
		// TODO Auto-generated method stub
		
		return (size==0)? null : head.min();
	}

	@Override
	public K max() {
		// TODO Auto-generated method stub
		return (size==0)? null : head.max();
	}

	@Override
	public void deleteMin() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteMax() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(K key) {
		// TODO Auto-generated method stub
		
	}
	
	private void getInorden(DoubleLinkedList<V> valores, NodeArbolRN<K,V> actual){
		
		if(actual == null)
			return;
		
		getInorden(valores, actual.getHijoIzquierdo());
		valores.addAtEnd(actual.getValor());
		getInorden(valores, actual.getHijoDerecho());

	}
	
	private void getInOrdenKeys(DoubleLinkedList<K> valores, NodeArbolRN<K,V> actual)
	{
		if(actual == null)
			return;
		
		getInOrdenKeys(valores, actual.getHijoIzquierdo());
		valores.addAtEnd(actual.getLlave());
		getInOrdenKeys(valores, actual.getHijoDerecho());
	}
	
	public void imprimirNodosOrdenPorNiveles()
	{
		if(size > 0)
			imprimirNodosOrdenPorNiveles(head, 0);
			
		
		
	}
	
	private void imprimirNodosOrdenPorNiveles(NodeArbolRN<K, V> actual, int lvl)
	{
		if(actual == null)
			return;
		
		System.out.println("����������");
		System.out.println("Nivel: "+lvl);
		System.out.println(actual.toString());
		
		
		imprimirNodosOrdenPorNiveles(actual.getHijoIzquierdo(), lvl+1);
		imprimirNodosOrdenPorNiveles(actual.getHijoDerecho(), lvl+1);
			
		
		
	}
}