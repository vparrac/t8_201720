package model.data_structures;

public class Vertice<K extends Comparable<K>, V>{
	
	private K key;
	private V value;
	private IList<Edge<K>> edges;
	private int degree;
	
	public Vertice(K key, V value)
	{
		this.key = key;
		this.value = value;
		edges = new DoubleLinkedList<Edge<K>>();
		degree = 0;
		
	}
	
	public K getKey()
	{
		return key;
	}
	
	public V getValue()
	{
		return value;
	}
	
	public int getDegree()
	{
		return degree;
	}
	
	public boolean containsEdge(K dest)
	{
		for(Edge<K> actual : edges)
			if(actual.getDest().equals(dest))
				return true;
		return false;
	}
	
	public void setValue(V value){
		this.value = value;
	}
	
	public boolean addEdge(Edge<K> newEdge)
	{
		if(!newEdge.getSource().equals(key))
			return false;
		
		if(edges.contains(newEdge))
			return false;
		
		//System.out.println(newEdge.toString());
		edges.addAtEnd(newEdge);
		degree++;
		return true;
	}
	
	public boolean deleteEdge(K dest)
	{
		int i = 0;
		for(Edge<K> actual : edges)
		{
			if(actual.getDest().equals(dest))
			{
				edges.deleteAtK(i);
				degree--;
				return true;
			}
				
			i++;
				
		}
		return false;		
	}
	
	public IList<Edge<K>> getEdges()
	{
		return edges;
	}
	
	public PriorityQueue<Edge<K>> getEdgesAsBinaryHeap()
	{
		PriorityQueue<Edge<K>> edgesHeap = new PriorityQueue<Edge<K>>(true);
		for(Edge<K> actual : edges)
		{
			actual.compararPorWeight();
			edgesHeap.enqueue(actual);
		}
			
		return edgesHeap;
		
	}
	
	public void deletAllEdges()
	{
		edges = new DoubleLinkedList<Edge<K>>();
		degree = 0;
	}
	
}
