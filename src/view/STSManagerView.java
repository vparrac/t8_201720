package view;

import java.util.Scanner;

import controller.Controller;
import model.Vo.VOParada;
import model.data_structures.ArbolRojoNegro;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Iterator;
public class STSManagerView {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();			
			int option = sc.nextInt();
			switch(option){

			case 1:
				try {
					Controller.cargar();
				}
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;

			case 2:
				try {
					System.out.println("El n�mero de componentes conectados es: "+Controller.contarComponentesConectados());

				}
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 3:
				try {
					System.out.println("El n�mero de componentes conectados es: "+Controller.contarComponentesConectados());
					IList<VOParada> lista = Controller.tieneCiclo();
					if(lista==null) System.out.println("El grafo no tiene ciclos");
					else{
						System.out.println("Las paradas en un ciclo del grafo:");
						for (VOParada voParada : lista) {
							System.out.println(voParada);
						}
					}

				}
				catch (Exception e) {
					System.err.println(e.getMessage());
					e.printStackTrace();
				}		
				break;
			case 4:
				try {
					System.out.println("Ingrese el id de la parada");
					int i = sc.nextInt();
					ArbolRojoNegro arbol=Controller.contarComponentesConectadosAUnaParada(i);

					Iterator<IList<Integer>> nodos = arbol.recorridoInorden();
					Iterator<Integer> distancias = arbol.keys();

					IList<Integer> actual;
					int distanciaActual; 
					System.out.println("Los componentes conectados a la parada y su respectiva sidtancia son: ");
					while(nodos.hasNext())
					{
						distanciaActual = distancias.next();

						actual = nodos.next();
						System.out.println("\nDistancia : " + distanciaActual);
						for(Integer characterActual : actual)
							System.out.print("<"+characterActual+">");
					}
				}


				catch (Exception e) {
					// TODO: handle exception
				}

				break;
			case 5:			
				fin=true;
				sc.close();
				break;
			}
		}
	}
	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 8----------------------");
		System.out.println("1. Cargue la informaci�n de las paradas en un grafo");
		System.out.println("2. Consulte cuantos componentes conectados tiene el grafo construido");
		System.out.println("3. Consulte dada una parada, cuales son su componentes conectados");

	}
}
