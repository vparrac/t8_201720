package controller;
import api.ISTSManager;
import model.Vo.VOParada;
import model.data_structures.ArbolRojoNegro;
import model.data_structures.IList;
import model.logic.STSManager;	
public class Controller {
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ISTSManager manager  = new STSManager();
	
	public static void cargar() throws Exception{
		manager.cargar();
	}
	public static int contarComponentesConectados(){
		return manager.contarComponentesConectados();
	}
	public static ArbolRojoNegro contarComponentesConectadosAUnaParada(int idParada){
		return manager.componentesConectadosAUnaParada(idParada);
	}
	public static IList<VOParada> tieneCiclo() {
		return manager.tieneCiclos();

	}

}