package model.logic;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.sound.sampled.Line;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import model.Vo.BusUpdateVO;
import model.Vo.VOAgency;
import model.Vo.VOCalendar;
import model.Vo.VODate;
import model.Vo.VOHour;
import model.Vo.VOParada;
import model.Vo.VOParadaViaje;
import model.Vo.VORuta;
import model.Vo.VOStatus;
import model.Vo.VOViaje;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Iterator;
import model.data_structures.LinearProbingHashST;
public class DataLoader {
	//Atributo de las paradas
	private LinearProbingHashST<Integer, VOParada> paradas;
	//Atributo para los viajes
	private LinearProbingHashST<Integer, VOViaje> viajes;
	//Atributo para las rutas
	private LinearProbingHashST<Integer, VORuta> rutas;
	//Agencia
	private DoubleLinkedList<VOAgency> agencias;
	//Atributo para el calendar
	private DoubleLinkedList<VOCalendar> calendar;

	private LinearProbingHashST<Integer, BusUpdateVO> busesDia22;
	private LinearProbingHashST<Integer, BusUpdateVO> busesDia21;
	/**
	 * Constructor
	 */
	public DataLoader(){
		busesDia21= new LinearProbingHashST<Integer, BusUpdateVO>();
		busesDia22 = new LinearProbingHashST<Integer, BusUpdateVO>();
		System.gc();
	}

	//----------------------------------------------------
	//Json
	//----------------------------------------------------

	public void readStopsEstim(File rtFile, int day) throws JsonIOException, JsonSyntaxException, FileNotFoundException{
		JsonParser jsonParser = new JsonParser();
		JsonElement jsonElement = jsonParser.parse(new FileReader(rtFile));
		JsonArray jsonArray = jsonElement.getAsJsonArray();
		JsonObject jObjectActual, rutaActual = null;
		VOParada paradaActual;
		VORuta ruta;
		JsonArray schedules;
		for(JsonElement jElementActual : jsonArray){
			rutaActual = jElementActual.getAsJsonObject();
			//Busca la parada, la obtiene del nombre del archivo que le pasan por par�metro
			
			paradaActual = buscarParada(rtFile.getName().split("_")[3].split("-")[0]);
			jObjectActual = jElementActual.getAsJsonObject();			
			//Busca la ruta
			ruta = buscarRuta(jObjectActual.getAsJsonPrimitive("RouteNo").getAsString());
			paradaActual.addRoute(ruta);
			//Segunda parte del Json
			schedules = rutaActual.getAsJsonArray("Schedules"); 
			JsonObject scheduleActual;
			//Lee los horarios de llegada -schedules- de la ruta actual.
			for(JsonElement elementoScheduleActual: schedules){
				scheduleActual = elementoScheduleActual.getAsJsonObject();										 
				leerSchedule(elementoScheduleActual, scheduleActual,day, paradaActual );			
			}
		}
	}

	public void readBusUpdate(File rtFile, int dia) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		JsonParser jsonParser = new JsonParser();
		LinearProbingHashST<Integer, BusUpdateVO> listaBus = (dia==21)? busesDia21: busesDia22;
		JsonElement jsonElement = jsonParser.parse(new FileReader(rtFile));
		JsonArray jsonArray = jsonElement.getAsJsonArray();
		JsonObject jObjectActual;
		Integer nVehiculo, idTrip;
		String nRuta,direccion, destino, pattern,tRegistrado;
		Double latitud,longitud;
		BusUpdateVO buscada, bus;
		for(JsonElement jElementActual : jsonArray){
			jObjectActual = jElementActual.getAsJsonObject();			
			//N�mero del veh�culo
			nVehiculo =jObjectActual.getAsJsonPrimitive("VehicleNo").getAsInt();
			//Trip ID				
			idTrip = new Integer( jObjectActual.getAsJsonPrimitive("TripId").getAsInt() );
			//N�mero de la ruta
			nRuta= jObjectActual.getAsJsonPrimitive("RouteNo").getAsString();
			//Direction
			direccion = jObjectActual.getAsJsonPrimitive("Direction").getAsString();
			//Destination				
			destino = jObjectActual.getAsJsonPrimitive("Destination").getAsString();
			//Placa			
			pattern = jObjectActual.getAsJsonPrimitive("Pattern").getAsString();				
			//Latitud
			latitud = jObjectActual.getAsJsonPrimitive("Latitude").getAsDouble();
			//Longitud				
			longitud = jObjectActual.getAsJsonPrimitive("Longitude").getAsDouble();
			//Tiempo 				
			tRegistrado = jObjectActual.getAsJsonPrimitive("RecordedTime").getAsString();
			buscada = buscarBus(nVehiculo, dia);
			VOViaje viaje= viajes.get(idTrip);			 
			if(buscada==null){						
				bus = new BusUpdateVO(nVehiculo, idTrip, nRuta, direccion, destino, pattern, latitud, longitud);						
				//A�ade la hora al bus, se a�ade en la misma posici�n en la que esta su posici�n
				bus.anadirHora(tRegistrado);						
				listaBus.put(nVehiculo, bus);
				viaje.anadirBus(bus,dia);
			}				
			else{					
				//Se manejara formato 24 horas					
				buscada.anadirHora(tRegistrado);
				buscada.anadirPosicion(latitud,longitud);				
			}				
		}			
	}	
	//---------------------------------------------------
	//Toda la informaci�n est�tica
	//---------------------------------------------------

	/**
	 * M�todo para cargar CalendarDates
	 */
	public void cargarCalendarDates(){
		try{//Inicializa la lista donde se guardar�n los calendarios
			//Lectura del archivo
			FileReader fr=new FileReader("./data/csv/calendar_dates.txt");
			VOCalendar buscado=null;
			BufferedReader br = new BufferedReader(fr);	String lin=br.readLine();
			lin=br.readLine();
			String[] ln;
			String lineaTiempo;
			VODate inicio;
			while(lin != null){			
				ln = lin.split(",");
				lineaTiempo = ln[1];
				inicio= new VODate(Integer.parseInt(lineaTiempo.substring(0, 4)), Integer.parseInt(lineaTiempo.substring(4,6)), Integer.parseInt(lineaTiempo.substring(6)));
				for (VOCalendar actual : calendar) {
					buscado=actual;
					if(actual.getServiceId()==Integer.parseInt(ln[0])) break;
				}
				buscado.agregarExcepcion(inicio);	
				lin=br.readLine();
			}			
			br.close();
			fr.close();			
		}		
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * M�todo para cargar las Agencias
	 */
	public void cargarAgencias(){		
		agencias = new DoubleLinkedList<VOAgency>();
		try (BufferedReader br = new BufferedReader(new FileReader("./data/csv/agency.txt"))){		
			String lin=br.readLine();
			lin=br.readLine();
			String[] linea;
			VOAgency nueva;
			while(lin!=null){				
				linea = lin.split(",");
				nueva = new VOAgency(linea[0], linea[1], linea[2], linea[3], linea[4]);
				agencias.addAtEnd(nueva);
				lin = br.readLine();				
			}			
		} 
		catch (Exception e) {
			System.err.println("Error: DataLoader.cargarAgencias() " + e.getCause());
		}
	}	
	/**
	 * Cargar Calendar
	 */
	public void cargarCSVCalendar() {
		try{
			//Inicializa la lista donde se guardar�n los calendarios
			calendar= new DoubleLinkedList<VOCalendar>();
			//Lectura del archivo
			FileReader fr=new FileReader("./data/csv/calendar.txt");
			BufferedReader br = new BufferedReader(fr);	String lin=br.readLine();
			lin=br.readLine();
			String[] ln;
			Integer[] dias ;
			String lineaTiempo;
			VODate inicio,fin;
			VOCalendar nuevo;
			while(lin != null){			
				ln = lin.split(",");
				dias = cargarDias(lin);	
				lineaTiempo = ln[8];
				inicio= new VODate(Integer.parseInt(lineaTiempo.substring(0, 4)), Integer.parseInt(lineaTiempo.substring(4,6)), Integer.parseInt(lineaTiempo.substring(6)));
				lineaTiempo = ln[9];
				fin= new VODate(Integer.parseInt(lineaTiempo.substring(0, 4)), Integer.parseInt(lineaTiempo.substring(5,6)), Integer.parseInt(lineaTiempo.substring(7)));
				nuevo = new VOCalendar(Integer.parseInt(ln[0]), dias, inicio, fin);
				//Crea los calendarios
				calendar.addAtEnd(nuevo);
				lin=br.readLine();
			}			
			br.close();
			fr.close();	
		}		
		catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * M�todo para cargar las paradas
	 */
	public void cargarParadas(){
		paradas = new LinearProbingHashST<Integer, VOParada>(3001);	
		try (BufferedReader br = new BufferedReader(new FileReader("./data/csv/stops.txt"))){		
			String lin=br.readLine();
			lin=br.readLine();
			String[] linea;
			VOParada nueva;
			while(lin!=null){				
				linea = lin.split(",");
				nueva = new VOParada(Integer.parseInt(linea[0]), linea[2], linea[1]);
				nueva.setLatitud(Double.parseDouble(linea[4]));
				nueva.setLongitud(Double.parseDouble(linea[5]));
				//La llave ser� el ID
				paradas.put(new Integer(nueva.getStopId()), nueva);
				lin = br.readLine();
			}			
		} 
		catch (Exception e) {
			System.err.println("Error: DataLoader.cargarParadas() " + e.getCause());
		}		
	}
	/**
	 * M�todo para cargar las rutas
	 */
	public void cargarRutas(){
		rutas = new LinearProbingHashST<>();
		try (BufferedReader br = new BufferedReader(new FileReader("./data/csv/routes.txt"))){		
			String lin=br.readLine();
			VOAgency agencia= null;
			lin=br.readLine();
			String[] linea;
			VORuta nueva;
			while(lin!=null){							
				linea = lin.split(",");
				for (VOAgency agencita :agencias) {
					agencia= agencita;
					if(agencita.getId().equals(linea[1])) break;
				}				
				nueva = new VORuta(Integer.parseInt(linea[0]), agencia, linea[2], linea[3], Integer.parseInt(linea[5])); 
				//La llave ser� el ID
				rutas.put(nueva.getIdRoute(), nueva);				
				lin = br.readLine();
			}			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void cargarViajes(){	
		viajes = new LinearProbingHashST<Integer, VOViaje>();	
		try (BufferedReader br = new BufferedReader(new FileReader("./data/csv/trips.txt"))){
			String lin=br.readLine();
			lin=br.readLine();
			String[] linea;
			VOViaje nuevo;
			VORuta rutaAsociada;
			while(lin!=null){				
				linea = lin.split(",");				
				nuevo = new VOViaje(Integer.parseInt(linea[2]), linea[3], Integer.parseInt(linea[0]));
				viajes.put(Integer.parseInt(linea[2]), nuevo);		
				rutaAsociada = rutas.get(Integer.parseInt(linea[0]));
				rutaAsociada.agregarTrip(nuevo);
				lin=br.readLine();
			}
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}	
	public void cargarStopTimes(){
		try (BufferedReader br = new BufferedReader(new FileReader("./data/csv/stop_times.txt"))){				
			String lin=br.readLine();
			VOViaje ultimo =viajes.get(9017927);
			double ultimaDistancia =0;
			lin=br.readLine();
			String[] linea,lineaTiempo;
			VOHour horaNueva;
			Integer idViaje;
			VOViaje viajeActual;
			VOParada paradaActual;
			VOParadaViaje nuevaParada;
			VORuta ruta;
			while(lin!=null){		

				linea = lin.split(",");
				lineaTiempo = linea[1].split(":");
				horaNueva = new VOHour( Integer.parseInt(lineaTiempo[0].trim()), Integer.parseInt(lineaTiempo[1].trim()), Integer.parseInt(lineaTiempo[2].trim()));				
				if(Integer.parseInt(linea[4].trim())==1)
					ultimo.setMaximaDistancia(ultimaDistancia);				
				if(linea.length==9)
					ultimaDistancia= Double.parseDouble(linea[8].trim());

				idViaje = Integer.parseInt(linea[0]);
				viajeActual = viajes.get(idViaje);				
				paradaActual = paradas.get(Integer.parseInt(linea[3]));
				nuevaParada =  new VOParadaViaje(Integer.parseInt(linea[3]), paradaActual.getStop_name(), paradaActual.getStop_code(), paradaActual);
				ruta = rutas.get(viajeActual.getRouteId());
				nuevaParada.setArrivalTime(horaNueva);
				viajeActual.anadirParada(nuevaParada);
				//Mismo orden que las paradas				
				paradaActual.addTrip(viajeActual);
				paradaActual.addRoute(ruta);				
				ultimo = viajes.get(idViaje);
				lin = br.readLine();

			}
		}		
		catch (NullPointerException np) {
			System.err.println("Para cargar el stop times, primero cargue los viajes y las paradas");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}	
	public LinearProbingHashST<Integer, VOParada> getParadas() {
		return paradas;
	}
	public  LinearProbingHashST<Integer, VOViaje> getViajes() {
		return viajes;
	}



	//---------------------------------------------
	//M�todos auxiliares
	//---------------------------------------------

	public LinearProbingHashST<Integer, VORuta> getRutas() {
		return rutas;
	}

	public DoubleLinkedList<VOAgency> getAgencias() {
		return agencias;
	}

	public DoubleLinkedList<VOCalendar> getCalendar() {
		return calendar;
	}

	public LinearProbingHashST<Integer, BusUpdateVO> getBusesDia22() {
		return busesDia22;
	}

	public LinearProbingHashST<Integer, BusUpdateVO> getBusesDia21() {
		return busesDia21;
	}

	private VOParada buscarParada(String stopCode){
		//DoubleLinkedList<VOParada> parada = paradas.list();
		for(Integer paradita: paradas){
			if(paradas.get(paradita).getStop_code().equals(stopCode)){
				return paradas.get(paradita);
			}
		}
		return null;
	}

	private Integer[] cargarDias(String linea){
		String[] ln = linea.split(",");	
		Integer[] dias = new Integer[7];
		for (int i = 1; i <= dias.length; i++) {
			dias[i-1]= Integer.parseInt(ln[i]);
		}
		return dias;
	}	

	private BusUpdateVO buscarBus(Integer noVehiculo, int dia){
		LinearProbingHashST<Integer, BusUpdateVO> buses = (dia==21)? busesDia21: busesDia22;
		BusUpdateVO bus = null;		
		for (Integer integer : buses) {
			if(buses.get(integer).getnVehiculo().equals(noVehiculo)){
				bus=buses.get(integer);
				break;
			}
		}		
		return bus;
	}
	private VORuta buscarRuta(String routeShortName){
		for (Integer voRuta : rutas) {
			if(rutas.get(voRuta).getShortName().equals(routeShortName)){
				return rutas.get(voRuta);
			}
		}
		return null;
	}
	public BusUpdateVO buscarBus(String pattern, int dia){
		LinearProbingHashST<Integer, BusUpdateVO> buses = (dia==21)? busesDia21: busesDia22;
		BusUpdateVO bus = null;
		for (Integer busUpdateVO : buses) {
			if(buses.get(busUpdateVO).getPattern().equals(pattern)){
				bus=buses.get(busUpdateVO);
			}
		}
		return bus;		
	}
	private void leerSchedule(JsonElement elementoScheduleActual, JsonObject scheduleActual, int day, VOParada paradaActual){
		scheduleActual = elementoScheduleActual.getAsJsonObject();
		String sPattern = scheduleActual.getAsJsonPrimitive("Pattern").getAsString();
		String sExpectedLeaveTime = scheduleActual.getAsJsonPrimitive("ExpectedLeaveTime").getAsString();
		String sScheduleStatus = scheduleActual.getAsJsonPrimitive("ScheduleStatus").getAsString();
		String sLastUpdate = scheduleActual.getAsJsonPrimitive("LastUpdate").getAsString();
		BusUpdateVO busesito = buscarBus(sPattern, day);

		if(busesito==null)
			System.err.println(sPattern + "Bus no encontrado");
		else{
			if(!sScheduleStatus.equals(" ")){
				VOStatus nuevo = new VOStatus(sScheduleStatus, sExpectedLeaveTime, sLastUpdate, paradaActual);
				busesito.anadirStatus(nuevo);
				if(sScheduleStatus.equals("+")){
					paradaActual.anadirRetardo();
				}
			}
		}
	}

	public void cargarTiemposTrasferencia(){
		try (BufferedReader br = new BufferedReader(new FileReader("./data/csv/transfers.txt"))){		
			String lin=br.readLine();
			lin=br.readLine();
			String[] linea;
			while(lin!=null){							
				linea = lin.split(",");
				if(linea[2].equals("")){
					paradas.get(Integer.parseInt(linea[0].trim())).agregarTiempo(Integer.parseInt(linea[1].trim()), 0);
				}else{
					paradas.get(Integer.parseInt(linea[0].trim())).agregarTiempo(Integer.parseInt(linea[1].trim()), Integer.parseInt(linea[2].trim()));
				}
				lin = br.readLine();
			}			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}