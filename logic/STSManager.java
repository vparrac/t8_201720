package model.logic;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.NoSuchElementException;

import model.Vo.VODate;
import model.Vo.VOHour;
import model.Vo.VOParada;
import model.Vo.VOParadaViaje;
import model.Vo.VORuta;
import model.Vo.VOTransbordo;
import model.Vo.VOViaje;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.LinearProbingHashST;
import model.data_structures.PriorityQueue;
import model.data_structures.Tree2_3;
import api.ISTSManager;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
public class STSManager implements ISTSManager{
	private  DataLoader cargador;
	public STSManager() {
		cargador = new DataLoader();
	}
	@Override
	public void cargarParadas() {
		cargador.cargarParadas();
	}
	@Override
	public void cargarViajes() {
		cargador.cargarViajes();		
	}
	@Override
	public void cargarStopTimes() {
		cargador.cargarStopTimes();		
	}
	@Override
	public void cargarAgencias() {
		cargador.cargarAgencias();
	}
	@Override
	public void cargarCalendar() {
		cargador.cargarCSVCalendar();		
	}
	@Override
	public void cargarCalendarDates() {
		cargador.cargarCalendarDates();
	}
	@Override
	public void cargarRutas() {
		cargador.cargarRutas();	
	}
	@Override
	public void cargarBusService() throws JsonIOException, JsonSyntaxException, FileNotFoundException {		
		File f = new File("./data/RealTime-8-21-BUSES_SERVICE");		
		File[] updateFiles = f.listFiles();		
		int n =0;
		for (int i = 0; i < updateFiles.length; i++) {	
			System.out.println(n++);
			System.out.println(updateFiles[i]);
			cargador.readBusUpdate(updateFiles[i],21);
		}	
				f = new File("./data/RealTime-8-22-BUSES_SERVICE");	
				updateFiles = f.listFiles();	
				n =0;
				for (int i = 0; i < updateFiles.length; i++) {	
					System.out.println(n++);
					System.out.println(updateFiles[i]);
					cargador.readBusUpdate(updateFiles[i],22);
				}

	}
	@Override
	public void cargarStimBusService() throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		File f = new File("./data/RealTime-8-21-STOPS_ESTIM_SERVICES");
		int n =0;
		File[] updateFiles = f.listFiles();				
		for (int i = 0; i < updateFiles.length; i++) {	
			System.out.println(n++);
			System.out.println(updateFiles[i]);
			cargador.readStopsEstim(updateFiles[i],21);
		}
				n=0;
				f = new File("./data/RealTime-8-22-STOPS_ESTIM_SERVICES");
				updateFiles = f.listFiles();				
				for (int i = 0; i < updateFiles.length; i++) {	
					System.out.println(n++);
					System.out.println(updateFiles[i]);
					cargador.readStopsEstim(updateFiles[i],22);
				}	
	}
	
	//-----------------------BEGIN--------------------------------------------

	
	@Override
	public VORuta viajesConRetardo1a(String fecha, Integer idRuta){
		if(!(fecha.equalsIgnoreCase("20170821")||(fecha.equalsIgnoreCase("20170822")))){	
			System.err.println("No hay datos de la fecha suministrada");
			throw new NoSuchElementException();
		}
		else{
			VORuta rutita = cargador.getRutas().get(idRuta);
			rutita.metodo1A(queDiaEs(fecha));
			return rutita;
		}
	}
	
	@Override
	public Tree2_3<Integer, PriorityQueue<VOParada>> ITSViajesConRetardosDeIAN1B (int routeId, String fechaString)throws Exception {
		// TODO Auto-generated method stub
		VORuta buscada = cargador.getRutas().get(routeId);
		if(buscada == null)
			throw new Exception("La ruta buscada con el id no existe.");
		
		if(!(fechaString.equals(20170821+"")||fechaString.equals(20170822+"")))
			throw new Exception("Fecha fuera de rango.");
		//Se verifica que la fecha sea una de las dos fechas en las que se tiene informaci�n de buses, retardos, etc.
		//Se supone que los json de buses 21 y 22 ya est�n cargados. 
		String dia = String.valueOf( fechaString.charAt(6)) + String.valueOf( fechaString.charAt(7));
		String mes = String.valueOf( fechaString.charAt(4)) + String.valueOf( fechaString.charAt(5));
		String anho = String.valueOf( fechaString.charAt(0)) + String.valueOf( fechaString.charAt(1)) + String.valueOf( fechaString.charAt(2)) + String.valueOf( fechaString.charAt(3));
		
		VODate fecha = new VODate(Integer.parseInt(anho), Integer.parseInt(mes), Integer.parseInt(dia));
		
		Tree2_3<Integer, PriorityQueue<VOParada>> viajesConRetardosConsecutivos = new Tree2_3<Integer, PriorityQueue<VOParada>>();
		IList<VOViaje> viajes = buscada.getTrips().list();
		
		
		PriorityQueue<VOParada> paradasViaje;
		for(VOViaje viajeActual : viajes)
		{
			paradasViaje = viajeActual.verificarSiRetardoDeIAN1B(fecha);
			if(paradasViaje != null)
			{
				viajesConRetardosConsecutivos.insert(viajeActual.ID(), paradasViaje);
			}
		}
		
		return viajesConRetardosConsecutivos;
	}
	public Tree2_3<VOHour, VOParada> ITSNRangoHorasMasRetardos2B(String routeId, String fechaString)throws Exception
	{
		if(!(fechaString.equals(20170821+"")||fechaString.equals(20170822+"")))
			throw new Exception("Fecha fuera de rango.");
		//Se verifica que la fecha sea una de las dos fechas en las que se tiene informaci�n de buses, retardos, etc.
		//Se supone que los json de buses 21 y 22 ya est�n cargados. 
		String dia = String.valueOf( fechaString.charAt(6)) + String.valueOf( fechaString.charAt(7));
		String mes = String.valueOf( fechaString.charAt(4)) + String.valueOf( fechaString.charAt(5));
		String anho = String.valueOf( fechaString.charAt(0)) + String.valueOf( fechaString.charAt(1)) + String.valueOf( fechaString.charAt(2)) + String.valueOf( fechaString.charAt(3));
		
		VODate fecha = new VODate(Integer.parseInt(anho), Integer.parseInt(mes), Integer.parseInt(dia));
		//Se busca la ruta con el id.
		VORuta ruta = cargador.getRutas().get(Integer.parseInt(routeId));
		if(ruta == null)
			throw new Exception("La ruta con el id dado por par�metro no existe.");
		
		return ruta.rangoHoraMasParadasRetrazoB2(fecha);
					
	}
	
	
	
	public IList<VOViaje> ITSNViajesDeOrigenADestino3B(int idInitStop, int idEndStop, String sInitHour, String sEndHour, String fecha)
	{
		IList<VOViaje> resultado = new DoubleLinkedList<VOViaje>();
		int diaSemana = queDiaSemanaCaeFecha(fecha);
		
		VOParada initStop = cargador.getParadas().get(idInitStop);
		if(initStop == null)
			return null;
		
		String horaInicio[] = sInitHour.split(":");
		String horaFinal[] = sEndHour.split(":");
		
		VOHour initHour = new VOHour(Integer.parseInt(horaInicio[0]), Integer.parseInt(horaInicio[1]), Integer.parseInt(horaInicio[2]));
		VOHour endHour = new VOHour(Integer.parseInt(horaFinal[0]), Integer.parseInt(horaFinal[1]), Integer.parseInt(horaFinal[2]));
		
		
		DoubleLinkedList<VOViaje> viajesInitStop = initStop.getViajes().list();
		
		for(VOViaje viajeActual : viajesInitStop)
		{
			
			VOParadaViaje pInit = viajeActual.getParadas().get(idInitStop);
			VOParadaViaje pEnd = viajeActual.getParadas().get(idInitStop);
			//if(rutaActual.)
			if(pInit != null && pInit.getArrivalTime().compareTo(initHour) >= 0 && pInit.getArrivalTime().compareTo(endHour) <= 0)
				if(pEnd != null && pEnd.getArrivalTime().compareTo(initHour) >= 0 && pEnd.getArrivalTime().compareTo(endHour) <= 0)
					resultado.addAtEnd(viajeActual);
					
		}
		return resultado;
	}
	@Override
	public VOViaje[] ITSNViajesMasDistancia2C(int n, String fecha) {
		if(fecha.equals(20170821+"")||fecha.equals(20170822+"")){
			PriorityQueue<VOViaje> cola = new PriorityQueue<>(true);
			DoubleLinkedList<VOViaje> viajes = cargador.getViajes().list();
			VOViaje[] retorno = new VOViaje[n];	
			for (VOViaje voViaje : viajes) {
				voViaje.OrdenarPorDistancia();
				cola.enqueue(voViaje);
			}
			for (int i = 0; i < retorno.length; i++) {
				retorno[i] = cola.dequeue();
			}
			return retorno;
		}
		else{
			return null;
		}	
	}
	public Tree2_3<VOHour, IList<VOParada>> ITSNRetardosDeViaje3C(String fechaString, int tripId)throws Exception
	{
		
		if(fechaString.equals(20170821+"")||fechaString.equals(20170822+"")){
			VOViaje viaje = cargador.getViajes().get(tripId);
			
			String dia = String.valueOf( fechaString.charAt(6)) + String.valueOf( fechaString.charAt(7));
			String mes = String.valueOf( fechaString.charAt(4)) + String.valueOf( fechaString.charAt(5));
			String anho = String.valueOf( fechaString.charAt(0)) + String.valueOf( fechaString.charAt(1)) + String.valueOf( fechaString.charAt(2)) + String.valueOf( fechaString.charAt(3));
			
			VODate fecha = new VODate(Integer.parseInt(anho), Integer.parseInt(mes), Integer.parseInt(dia));
			
			return viaje.getRetardosDeParadas(fecha);
		}
		else
		{
			throw new Exception("La fecha introducida est� fuera del rango de fechas permitidas.");
		}
	}
	@Override
	public VOParada ITSNParadaCompartida4C(int idParada) {
		return cargador.getParadas().get(new Integer(idParada));
	}
	@Override
	public DoubleLinkedList<VOViaje> ITSNViajesQueRealizaronParada5C(String fecha, String inicial, String horaFinal,int idRuta) {
		
		DoubleLinkedList<VOViaje> viajes = null;
		
		System.out.println("A�o: " + fecha.substring(0,4));
		System.out.println("Mes: " + fecha.substring(4,6));
		System.out.println("D�a: " + fecha.substring(6));
		if(Integer.parseInt(fecha.substring(0,4))==2017&&Integer.parseInt(fecha.substring(4,6))==8&&Integer.parseInt(fecha.substring(6))==21){
			viajes = cargador.getRutas().get(idRuta).getViajesRealizaronParadas5C(21, crearHoras(inicial), crearHoras(horaFinal));
		}
		else if (Integer.parseInt(fecha.substring(0,4))==2017&&Integer.parseInt(fecha.substring(4,6))==8&&Integer.parseInt(fecha.substring(6))==22){
			viajes = cargador.getRutas().get(idRuta).getViajesRealizaronParadas5C(22, crearHoras(inicial), crearHoras(horaFinal));
		}
		else{
			System.err.println("NO HAY DATOS DE LA FECHA INGRESADA");
		}
		
		return viajes;
	}	
	//------------------------------------------
	// M�todos auxiliares
	//------------------------------------------
	private VOHour crearHoras(String lineaTiempo){
		String[] partes = lineaTiempo.split(":");
		return new VOHour(Integer.parseInt(partes[0]),Integer.parseInt(partes[1]),Integer.parseInt(partes[2]));		
	}
	
	private void instanciarArbol(IList<VOParadaViaje> stopTimes, Tree2_3<VOHour, VOParadaViaje> paradasViaje) {
		for(VOParadaViaje stopActual : stopTimes)
			if(stopActual != null)
				paradasViaje.insert(stopActual.getArrivalTime(), stopActual);
	}
	private int queDiaSemanaCaeFecha(String fecha)
	{
		int anho = Integer.parseInt(fecha.substring(0, 4)),mes =Integer.parseInt(fecha.substring(5,6)), dia= Integer.parseInt(fecha.substring(7));
		Calendar ahoraCal = Calendar.getInstance();
		ahoraCal.set(anho,mes,dia);
		return (Calendar.DAY_OF_WEEK-1);
	}
	private int queDiaEs (String fecha){
		return new Integer(Integer.parseInt(fecha.substring(7)));
	}
	public DoubleLinkedList<VOTransbordo> getTransbordos(Integer Idruta){
		VORuta ruta = cargador.getRutas().get(Idruta);
		DoubleLinkedList<VOTransbordo> list = new DoubleLinkedList<VOTransbordo>();
		LinearProbingHashST<Integer,VOViaje> viajes =ruta.getTrips();
		VOTransbordo transbordo; 
		for (Integer integer : viajes) {
			VOViaje viaje=ruta.getTrips().get(integer);
			LinearProbingHashST<Integer, VOParadaViaje> paradas = viaje.getParadas();
			for (Integer integer2 : paradas) {
				VOParadaViaje parada= viaje.getParadas().get(integer2);
				VOParada mom= parada.getMom();				
				if(mom.getRutas().size()==0){
					break;
				}
				else{
					transbordo = new VOTransbordo();
					transbordo.setIdParadaOrigen(parada.getStopId());
					LinearProbingHashST<Integer, VORuta> rutas = mom.getRutas();
					for (Integer integer3 : rutas) {
						transbordo.setListadeViaje(rutas.get(integer3));
						list.add(transbordo);
					}
				}
			}
			
		}
	
			
		
		return list;
	}
	@Override
	public PriorityQueue<VOParada> getNParadasConMasRetardos(int n, String fecha) {
		LinearProbingHashST<Integer, VOParada> iter =cargador.getParadas();
		PriorityQueue<VOParada> heap = new PriorityQueue<VOParada>(true);
		for (Integer integer : iter) {
			heap.enqueue(cargador.getParadas().get(integer));
		}
	
		return heap;
	}
	@Override
	public void cargartiempos()
	{
		cargador.cargarTiemposTrasferencia();
	}	
}