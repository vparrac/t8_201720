package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.ArbolRojoNegro;
import model.data_structures.DirectedGraph;
import model.data_structures.IList;
import model.data_structures.Iterator;
import model.data_structures.Paths;

public class PathsTest extends TestCase{
	
	private Character[] llaves;
	private String[] valores;
	private double[] pesos;
	private Character[][] arcos;
	
	private DirectedGraph<Character, String> grafo;
	private Paths<Character, String> paths;
	
	public PathsTest()
	{
		llaves = new Character[8];
		llaves[0] = 'A';
		llaves[1] = 'B';
		llaves[2] = 'C';
		llaves[3] = 'D';
		llaves[4] = 'E';
		llaves[5] = 'F';
		llaves[6] = 'G';
		llaves[7] = 'H';
		
		valores = new String[8];
		valores[0] = "Aurora";
		valores[1] = "Brayan";
		valores[2] = "Camilo";
		valores[3] = "Diana";
		valores[4] = "Estela";
		valores[5] = "Fabian";
		valores[6] = "Gabriel";
		valores[7] = "Hern�n";
		
		pesos = new double[12];
		pesos[0] = 2;
		pesos[1] = 2;
		pesos[2] = 2;
		pesos[3] = 1;
		pesos[4] = 3;
		pesos[5] = 2;
		pesos[6] = 1;
		pesos[7] = 1;
		pesos[8] = 3;
		pesos[9] = 1;
		pesos[10] = 1;
		pesos[11] = 1;
		
		arcos = new Character[12][2];
		arcos[0][0] = 'A';
		arcos[0][1] = 'C';
		arcos[1][0] = 'A';
		arcos[1][1] = 'D';
		arcos[2][0] = 'B';
		arcos[2][1] = 'D';
		arcos[3][0] = 'B';
		arcos[3][1] = 'E';
		arcos[4][0] = 'C';
		arcos[4][1] = 'G';
		arcos[5][0] = 'D';
		arcos[5][1] = 'B';
		arcos[6][0] = 'D';
		arcos[6][1] = 'F';
		arcos[7][0] = 'F';
		arcos[7][1] = 'B';
		arcos[8][0] = 'G';
		arcos[8][1] = 'F';
		arcos[9][0] = 'H';
		arcos[9][1] = 'G';
		arcos[10][0] = 'F';
		arcos[10][1] = 'E';
		arcos[11][0] = 'E';
		arcos[11][1] = 'B';
		
		
	}
	
	public void setupEscenario1()
	{
		grafo = new DirectedGraph<Character, String>();
		for(int i = 0; i < llaves.length; i++)
		{
			grafo.addVertice(llaves[i], valores[i]);
		}
		for(int i = 0; i < arcos.length; i++)
			grafo.addEdge(arcos[i][0], arcos[i][1], pesos[i]);
	}
	public void setupEscenario2()
	{
		setupEscenario1();
		paths = new Paths<Character, String>(grafo, llaves[0]);
	}
	
	public void setupEscenario3()
	{
		System.out.println("******************"
				+ "\nESCENARIO 3"
				+ "\n******************");
		setupEscenario2();
		paths.generatePaths();
	}
	
	
	
	public void testInit()
	{
		setupEscenario2();
	}
	
	public void GeneratePaths()
	{
		setupEscenario2();
		paths.generatePaths();
	}
	
	public void testPathsSearches()
	{
		setupEscenario3();
		
		assertTrue(paths.hasPathTo(llaves[1]));
		assertTrue(paths.hasPathTo(llaves[2]));
		assertTrue(paths.hasPathTo(llaves[3]));
		assertTrue(paths.hasPathTo(llaves[4]));
		assertTrue(paths.hasPathTo(llaves[5]));
		assertTrue(paths.hasPathTo(llaves[6]));
		assertFalse(paths.hasPathTo(llaves[7]));
		
		System.out.println("�������������������������������");
		System.out.println("paths To:");
		
		IList<Character> pathC = paths.pathTo(llaves[2]);
		assertTrue(pathC.getElement(0).equals(llaves[0]));
		assertTrue(pathC.getElement(1).equals(llaves[2]));
		printPath(pathC, llaves[2]);
		
		IList<Character> pathE = paths.pathTo(llaves[4]);
		assertTrue(pathE.getElement(0).equals(llaves[0]));
		assertTrue(pathE.getElement(1).equals(llaves[3]));
		assertTrue(pathE.getElement(2).equals(llaves[1]));
		assertTrue(pathE.getElement(3).equals(llaves[4]));
		printPath(pathE, llaves[4]);
	}
	
	public void GeneratePathsH()
	{
		System.out.println("\nTEST H");
		setupEscenario1();
		paths = new Paths<Character, String>(grafo, llaves[7]);
		paths.generatePaths();
	}
	
	public void testArbolRutasPorDistancia()
	{
		setupEscenario2();
		paths.generatePaths();
		
		ArbolRojoNegro<Integer, IList<Character>> actual = paths.getRutas();
		assertFalse(actual.isEmpty());
		printRutasPorDistancias(actual);
	}
	
	private void printRutasPorDistancias(ArbolRojoNegro<Integer, IList<Character>> arbol)
	{
		Iterator<IList<Character>> nodos = arbol.recorridoInorden();
		Iterator<Integer> distancias = arbol.keys();
		
		IList<Character> actual;
		int distanciaActual; 
		
		while(nodos.hasNext())
		{
			distanciaActual = distancias.next();
			
			actual = nodos.next();
			System.out.println("\nDistancia : " + distanciaActual);
			for(Character characterActual : actual)
				System.out.print("<"+characterActual+">");
		}
	}
	
	private void printPath(IList<Character> path, Character key)
	{
		System.out.println("\nPath to: " + key.toString());
		
		for(Character actual : path)
			System.out.print("<" + actual.toString() + ">");
	}
	
	
	
	
}
