package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.DFS;
import model.data_structures.DirectedGraph;
import model.data_structures.IList;
import model.data_structures.Stack;

public class DFSTest extends TestCase{
	
	private Character[] llaves;
	private String[] valores;
	private double[] pesos;
	private Character[][] arcos;
	
	private DirectedGraph<Character, String> grafo;
	private DFS<Character, String> dfs;
	
	public DFSTest()
	{
		llaves = new Character[7];
		llaves[0] = 'A';
		llaves[1] = 'B';
		llaves[2] = 'C';
		llaves[3] = 'D';
		llaves[4] = 'E';
		llaves[5] = 'F';
		llaves[6] = 'G';
		
		valores = new String[7];
		valores[0] = "Aurora";
		valores[1] = "Brayab";
		valores[2] = "Camilo";
		valores[3] = "Diana";
		valores[4] = "Estela";
		valores[5] = "Fabian";
		valores[6] = "Gabriel";
		
		pesos = new double[7];
		pesos[0] = 1;
		pesos[1] = 2;
		pesos[2] = 2;
		pesos[3] = 1;
		pesos[4] = 3;
		pesos[5] = 1;
		pesos[6] = 1;
		
		arcos = new Character[7][2];
		arcos[0][0] = 'A';
		arcos[0][1] = 'B';
		arcos[1][0] = 'A';
		arcos[1][1] = 'D';
		arcos[2][0] = 'B';
		arcos[2][1] = 'C';
		arcos[3][0] = 'C';
		arcos[3][1] = 'E';
		arcos[4][0] = 'C';
		arcos[4][1] = 'B';
		arcos[5][0] = 'D';
		arcos[5][1] = 'C';
		arcos[6][0] = 'F';
		arcos[6][1] = 'G';
		
	}
	
	public void setupEscenario1()
	{
		grafo = new DirectedGraph<Character, String>();
		for(int i = 0; i < llaves.length; i++)
		{
			grafo.addVertice(llaves[i], valores[i]);
		}
		for(int i = 0; i < arcos.length; i++)
			grafo.addEdge(arcos[i][0], arcos[i][1], pesos[i]);
	}
	public void setupEscenario2()
	{
		setupEscenario1();
		dfs = new DFS<Character, String>(grafo, llaves[0]);
	}
	
	public void setupEscenario3()
	{
		System.out.println("******************"
				+ "\nESCENARIO 3"
				+ "\n******************");
		setupEscenario2();
		dfs.generatePaths();
	}
	
	public void setupEscenario4()
	{
		setupEscenario1();
		dfs = new DFS<Character, String>(grafo.reverse(), llaves[4]);
		dfs.generatePaths();
	}
	
	public void setupEscenario5()
	{
		
	}
	
	/**
	public void testInit()
	{
		setupEscenario2();
		dfs.imprimir();
	}
	
	
	public void testGeneratePaths()
	{
		System.out.println("같같같같같같같같같같같같같같같�");
		System.out.println("GeneratePaths");
		System.out.println("같같같같같같같같같같같같같같같�");
		setupEscenario2();
		dfs.generatePaths();
	}
	
	public void testPathsSearches()
	{
		System.out.println("같같같같같같같같같같같같같같같�");
		System.out.println("PathsSearches");
		System.out.println("같같같같같같같같같같같같같같같�");
		setupEscenario3();
		
		assertTrue(dfs.hasPathTo(llaves[0]));
		assertTrue(dfs.hasPathTo(llaves[1]));
		assertTrue(dfs.hasPathTo(llaves[2]));
		assertTrue(dfs.hasPathTo(llaves[3]));
		assertTrue(dfs.hasPathTo(llaves[4]));
		assertFalse(dfs.hasPathTo(llaves[5]));
		assertFalse(dfs.hasPathTo(llaves[6]));
		
		
		System.out.println("****************************");
		System.out.println("PathTo()");
		System.out.println();
		
		IList<Character> pathToB =  dfs.pathTo(llaves[1]);
		
		assertTrue(pathToB.size() == 2);
		assertTrue(pathToB.getElement(0).equals(llaves[0]));
		assertTrue(pathToB.getElement(1).equals(llaves[1]));
		printPath(pathToB, llaves[1]);
		
		IList<Character> pathToE = dfs.pathTo(llaves[4]);
		
		printPath(pathToE, llaves[4]);
		assertTrue(pathToE.size()==4);
		assertTrue(pathToE.getElement(0).equals(llaves[0]));
		assertTrue(pathToE.getElement(1).equals(llaves[1]));
		assertTrue(pathToE.getElement(2).equals(llaves[2]));
		assertTrue(pathToE.getElement(3).equals(llaves[4]));
		
	}
	*/
	
	public void testPostOrden()
	{
		setupEscenario4();
		System.out.println("//Test Post Orden :");
		Stack<Character> postOrden = dfs.getPostOrden();
		assertTrue(true);
		//System.out.println("Tama�o postorden: "+ postOrden.size());
		while(postOrden.seeNext() != null)
			System.out.print("<"+postOrden.pop().toString()+">");
		
		System.out.println("*************************************");
		System.out.println("Post Orden Inverso:");
		setupEscenario4();
		postOrden = dfs.postOrdenInverso();
		printPostOrden(postOrden);
	}
	
	
	private void printPostOrden(Stack<Character > postOrden)
	{
		System.out.println("같같같같같같같같같같같같같같�");
		System.out.println("Post-Orden:");
		while(postOrden.seeNext() != null)
			System.out.print("<"+postOrden.pop()+">");
	}
	private void printPath(IList<Character> path, Character key)
	{
		System.out.println("\nPath To "+key + ":");
		for(Character keyActual : path)
			System.out.print("<" + keyActual.toString() + ">");
	}
}
