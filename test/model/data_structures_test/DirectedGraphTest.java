package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.DirectedGraph;
import model.data_structures.Edge;
import model.data_structures.IList;

public class DirectedGraphTest extends TestCase{
	
	private Character[] llaves;
	private String[] valores;
	private double[] pesos;
	private Character[][] arcos;
	private DirectedGraph<Character, String> grafo;
	
	public DirectedGraphTest()
	{
		llaves = new Character[5];
		llaves[0] = 'A';
		llaves[1] = 'B';
		llaves[2] = 'C';
		llaves[3] = 'D';
		llaves[4] = 'E';
		valores = new String[5];
		valores[0] = "Aurora";
		valores[1] = "Brayan";
		valores[2] = "Camilo";
		valores[3] = "Daniel";
		valores[4] = "Estefanny";
		pesos = new double[7];
		pesos[0] = 1;
		pesos[1] = 3;
		pesos[2] = 4;
		pesos[3] = 2;
		pesos[4] = 2;
		pesos[5] = 3;
		pesos[6] = 3;
		
		arcos = new Character[7][2];
		arcos[0][0] = 'B';
		arcos[0][1] = 'D';
		arcos[1][0] = 'B';
		arcos[1][1] = 'A';
		arcos[2][0] = 'B';
		arcos[2][1] = 'E';
		arcos[3][0] = 'C';
		arcos[3][1] = 'D';
		arcos[4][0] = 'C';
		arcos[4][1] = 'E';
		arcos[5][0] = 'C';
		arcos[5][1] = 'B';
		arcos[6][0] = 'D';
		arcos[6][1] = 'C';
		
	}
	
	public void setupEscenario1()
	{
		grafo = new DirectedGraph<Character, String>();
	}
	
	public void setupEscenario2()
	{
		setupEscenario1();
		for(int i = 0; i < llaves.length; i++)
			grafo.addVertice(llaves[i], valores[i]);
		
		for(int i = 0; i < 7; i++)
			grafo.addEdge(arcos[i][0], arcos[i][1], pesos[i]);
	}
	public void testAdd()
	{
		System.out.println("TEST ADD");
		setupEscenario1();
		assertTrue(grafo.isEmpty());
		assertTrue(grafo.v() == 0);
		assertTrue(grafo.e() == 0);
		
		for(int i = 0; i < 5; i++)
		{
			grafo.addVertice(llaves[i], valores[i]);
			assertTrue(grafo.v() == i+1);
		}
		//Se agrega un v�rtice repetido
		grafo.addVertice(llaves[0], valores[2]);
		assertTrue(grafo.getValue(llaves[0]).equals(valores[2]));
		assertTrue(grafo.e() == 0);
		
		for(int i = 0; i < 7; i++)
		{
			grafo.addEdge(arcos[i][0], arcos[i][1], pesos[i]);
			assertTrue(grafo.e() == i+1);
		}
		//Se agrega un arco repetido.
		grafo.addEdge(arcos[0][0], arcos[0][1], pesos[0]);
		printEdges();
		assertTrue(grafo.e() == 7);
	}
	
	public void testGet()
	{
		System.out.println("����������������������������������������");
		System.out.println("TEST GET");
		setupEscenario2();
		
		assertFalse(grafo.isEmpty());
		assertTrue(grafo.v() == 5);
		assertTrue(grafo.e() == 7);
		
		IList<Edge<Character>> arcos;
		for(int i = 0; i < 5; i++)
		{
			arcos = grafo.getEdges(llaves[i]);
			if(i >= 1 && i <= 3)
			{
				assertFalse(arcos == null);
				assertFalse(arcos.size() == 0);
				assertFalse(arcos.isEmpty());
			}
			else
				assertTrue(arcos.isEmpty());
		}
		printEdges();
	}
	
	
	public void testDelete()
	{
		System.out.println("����������������������������������������");
		System.out.println("TEST DELETE");
		
		System.out.println("Arcos:");
		setupEscenario2();
		
		grafo.deleteEdge(llaves[2], llaves[4]);
		assertTrue(grafo.e() == 6);
		grafo.deleteEdge(llaves[1], llaves[3]);
		assertTrue(grafo.e() == 5);
		
		printEdges();
		
		
		System.out.println("\n");
		System.out.println("V�rtices:");
		grafo.deleteVertice(llaves[1]);
		
		assertTrue(grafo.v() == 4);
		assertTrue(grafo.e() == 2);
		printEdges();
		
		
		
	}
	
	/**
	 * arco.toString() =  <source> -|peso|-> <dest>
	 */
	private void printEdges()
	{
		System.out.println("Arcos:");
		System.out.println("------------------");
		IList<Edge<Character>> arcosVerticeActual;
		for(int i = 0; i < llaves.length; i++)
		{
			System.out.println("+++++++++++++");
			arcosVerticeActual = grafo.getEdges(llaves[i]);
			if(arcosVerticeActual != null)
				for(Edge<Character> arcoActual : arcosVerticeActual)
					System.out.println(arcoActual.toString());
		}
			
	}
}
