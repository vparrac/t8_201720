package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.Cycle;
import model.data_structures.DirectedGraph;
import model.data_structures.IList;

public class CycleTest extends TestCase{
	
	private Character[] llaves;
	private String[] valores;
	private double[] pesos;
	private Character[][] arcos;
	
	private DirectedGraph<Character, String> grafo;
	private Cycle<Character, String> cycle;
	
	public CycleTest()
	{
		llaves = new Character[8];
		llaves[0] = 'A';
		llaves[1] = 'B';
		llaves[2] = 'C';
		llaves[3] = 'D';
		llaves[4] = 'E';
		llaves[5] = 'F';
		llaves[6] = 'G';
		llaves[7] = 'H';
		
		valores = new String[8];
		valores[0] = "Aurora";
		valores[1] = "Brayab";
		valores[2] = "Camilo";
		valores[3] = "Diana";
		valores[4] = "Estela";
		valores[5] = "Fabian";
		valores[6] = "Gabriel";
		valores[7] = "Hisma";
		
		pesos = new double[11];
		pesos[0] = 1;
		pesos[1] = 2;
		pesos[2] = 2;
		pesos[3] = 1;
		pesos[4] = 3;
		pesos[5] = 1;
		pesos[6] = 1;
		pesos[7] = 2;
		pesos[8] = 1;
		pesos[9] = 2;
		pesos[10] = 1;
		
		arcos = new Character[11][2];
		arcos[0][0] = 'A';
		arcos[0][1] = 'B';
		arcos[1][0] = 'A';
		arcos[1][1] = 'D';
		arcos[2][0] = 'B';
		arcos[2][1] = 'C';
		arcos[3][0] = 'C';
		arcos[3][1] = 'E';
		arcos[4][0] = 'C';
		arcos[4][1] = 'B';
		arcos[5][0] = 'D';
		arcos[5][1] = 'C';
		arcos[6][0] = 'F';
		arcos[6][1] = 'G';
		arcos[7][0] = 'D';
		arcos[7][1] = 'B';
		arcos[8][0] = 'B';
		arcos[8][1] = 'A';
		arcos[9][0] = 'G';
		arcos[9][1] = 'F';
		arcos[10][0] = 'F';
		arcos[10][1] = 'E';
	}
	
	public void setupEscenario1()
	{
		grafo = new DirectedGraph<Character, String>();
		for(int i = 0; i < llaves.length; i++)
		{
			grafo.addVertice(llaves[i], valores[i]);
		}
		for(int i = 0; i < arcos.length; i++)
			grafo.addEdge(arcos[i][0], arcos[i][1], pesos[i]);
	}
	
	public void setupEscenario2()
	{
		setupEscenario1();
		cycle = new Cycle<Character, String>(grafo);
		
	}
	
	public void testInit()
	{
		setupEscenario2();
		cycle.sourceCycles();
	}
	
	public void testCyclesSearches()
	{
		System.out.println("\nTEST CYCLES SEARCHES");
		setupEscenario2();
		cycle.sourceCycles();
		
		assertTrue(cycle.hasCycles());
		assertTrue(cycle.nCycles() == 2);
		
		System.out.println("\n\nRecorrido Ciclos:");
		IList<Character> cicloA = cycle.getCycle(llaves[0]);
		assertTrue(cicloA.size() == 4);
		assertTrue(cicloA.contains('A'));
		assertTrue(cicloA.contains('B'));
		assertTrue(cicloA.contains('C'));
		assertTrue(cicloA.contains('D'));
		printCycle(cicloA, llaves[0]);
		
		IList<Character> cicloC = cycle.getCycle(llaves[2]);
		assertTrue(cicloA.size() == 4);
		assertTrue(cicloA.contains('A'));
		assertTrue(cicloA.contains('B'));
		assertTrue(cicloA.contains('C'));
		assertTrue(cicloA.contains('D'));
		printCycle(cicloC, llaves[2]);
		
		IList<Character> cicloF = cycle.getCycle(llaves[5]);
		assertTrue(cicloF.size() == 2);
		assertTrue(cicloF.contains('F'));
		assertTrue(cicloF.contains('G'));
		printCycle(cicloF, llaves[5]);
		
		//Ni la E ni la F est�n en ciclos.
		assertTrue(cycle.getCycle(llaves[7]) == null);
		assertTrue(cycle.getCycle(llaves[4]) == null);
	}

	private void printCycle(IList<Character> actualCycle, Character vertice)
	{
		System.out.println("\n\nCiclo " + vertice + ": ");
		for(Character verticeActual : actualCycle)
			System.out.print("<" + verticeActual.toString() + ">");
	}
}
