package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.CC;
import model.data_structures.DirectedGraph;
import model.data_structures.IList;

public class CCTest extends TestCase{
	
	private Character[] llaves;
	private String[] valores;
	private double[] pesos;
	private Character[][] arcos;
	
	private DirectedGraph<Character, String> grafo;
	private CC<Character,String> cc;
	
	public CCTest()
	{
		llaves = new Character[8];
		llaves[0] = 'A';
		llaves[1] = 'B';
		llaves[2] = 'C';
		llaves[3] = 'D';
		llaves[4] = 'E';
		llaves[5] = 'F';
		llaves[6] = 'G';
		llaves[7] = 'H';
		
		valores = new String[8];
		valores[0] = "Aurora";
		valores[1] = "Brayab";
		valores[2] = "Camilo";
		valores[3] = "Diana";
		valores[4] = "Estela";
		valores[5] = "Fabian";
		valores[6] = "Gabriel";
		valores[7] = "Hisma";
		
		pesos = new double[11];
		pesos[0] = 1;
		pesos[1] = 2;
		pesos[2] = 2;
		pesos[3] = 1;
		pesos[4] = 3;
		pesos[5] = 1;
		pesos[6] = 1;
		pesos[7] = 2;
		pesos[8] = 1;
		pesos[9] = 2;
		pesos[10] = 1;
		
		arcos = new Character[11][2];
		arcos[0][0] = 'A';
		arcos[0][1] = 'B';
		arcos[1][0] = 'A';
		arcos[1][1] = 'D';
		arcos[2][0] = 'B';
		arcos[2][1] = 'C';
		arcos[3][0] = 'C';
		arcos[3][1] = 'E';
		arcos[4][0] = 'C';
		arcos[4][1] = 'B';
		arcos[5][0] = 'D';
		arcos[5][1] = 'C';
		arcos[6][0] = 'F';
		arcos[6][1] = 'G';
		arcos[7][0] = 'D';
		arcos[7][1] = 'B';
		arcos[8][0] = 'B';
		arcos[8][1] = 'A';
		arcos[9][0] = 'G';
		arcos[9][1] = 'F';
		arcos[10][0] = 'F';
		arcos[10][1] = 'E';
	}
	
	public void setupEscenario1()
	{
		grafo = new DirectedGraph<Character, String>();
		for(int i = 0; i < llaves.length; i++)
		{
			grafo.addVertice(llaves[i], valores[i]);
		}
		for(int i = 0; i < arcos.length; i++)
			grafo.addEdge(arcos[i][0], arcos[i][1], pesos[i]);
	}
	
	public void setupEscenario2()
	{
		setupEscenario1();
		cc = new CC<Character,String>(grafo);
	}
	
	
	public void testInit()
	{
		setupEscenario2();
	}
	
	public void testGenerateConnectedComponents()
	{
		System.out.println("TEST GENERATE CONNECTED COMPONENTS:");
		setupEscenario2();
		cc.generateConnectedComponents();
		
		IList<IList<Character>> connectedComponents = cc.getConnectedComponents();
		assertTrue(connectedComponents.size() == 4);
		for(IList<Character> cComponentActual : connectedComponents)
		{
			if(cComponentActual.contains(llaves[0]))
				assertTrue(cComponentActual.size() == 4);
			else if(cComponentActual.contains(llaves[4]))
				assertTrue(cComponentActual.size() == 1);
			else if(cComponentActual.contains(llaves[5]))
				assertTrue(cComponentActual.size() == 2);
			else if(cComponentActual.contains(llaves[7]))
				assertTrue(cComponentActual.size() == 1);
		}
	}
}
